[[_TOC_]]

# Séminaire de programmation : initiation au langage Python

L'objectif de ce séminaire est de découvrir les concepts fondamentaux de la programmation informatique.

Un langage de programmation est, comme son nom l'indique, un langage permettant de "parler" avec une machine. Le terme machine peut désigner toutes sortes d'appareils "informatiques" : un ordinateur, un smartphone, un objet connecté...

Pour être plus exact, écrire un programme (on emploie souvent le terme "coder") consiste d'avantage à donner des ordres à une machine, que celle-ci devra ensuite exécuter.

Un ordinateur n'est pas "intelligent". Il ne fait qu'effectuer ce qu'un programme lui demande de faire.

Il existe de nombreux langages de programmation, ayant chacun leur domaine de prédilection et leurs particularités.

Parmi les plus utilisés, on peut citer le C/C++, le C#, le Java, le JavaScript, et donc, le Python.

## Python, c'est quoi ?

- C'est un langage de programmation inventé en 1989/90, à l'origine pour faire du *scripting* (Un script est un programme plus ou moins court, utilisé pour réaliser automatiquement des tâches 'répétitives').

- C'est un langage sous licence *open-source*. Cela signifie qu'il est utilisable gratuitement, y compris à des fins commerciales, et que le "code source" du projet est consultable sur Internet.

- C'est un langage interprété. Cela signifie qu'à chaque nouvelle exécution d'un programme/script Python, l'interpréteur Python va exécuter les instructions écrites dans ledit script. C'est à dire les "traduire" une par une pour que l'ordinateur les comprenne et puisse faire ce qui est demandé par le code Python. On trouve "à l'inverse" les langages compilés (comme le C/C++) ou le compilateur va "analyser" l'intégralité d'un code source pour donner une version traduite de ce dernier, compréhensible par la machine. Voici une analogie pour résumer :
	- Langage interprété : vous discutez avec une personne qui parle une langue que vous ne comprenez pas. Vous êtes accompagné d'un·e interprète (le mot est d'ailleurs bien choisi) qui vous traduit ses paroles "en direct", phrase par phrase.
	- Langage compilé : vous discutez avec cette même personne. La personne note tout ce qu'elle veut vous dire d'un coup, dans son application Google Translate. Vous récupérez son téléphone, sur lequel vous pouvez lire la traduction de tout ce qu'elle voulait vous dire.
	
    Un langage interprété présente un avantage : sa portabilité. Cela signifie que le programme pourra fonctionner sur n'importe quelle machine, si tant est que l’interpréteur Python est installé sur celle-ci.
    
    Mais cela présente aussi un inconvénient : sa performance. Même si celle-ci s'est beaucoup améliorée au fil des ans, un même programme sera plus performant en langage compilé (par exemple en C++) qu'en Python.

- C'est un langage orienté objet. La programmation orientée objet est un paradigme (une vision) de programmation. On utilise le terme objet pour désigner une entité qui représente quelque chose, un objet donc. Un objet peut généralement avoir des caractéristiques et des fonctionnalités. En Python, **tout est objet**.

Alors, pourquoi apprendre Python ? (Une partie) de la réponse dans cette brève vidéo : https://www.youtube.com/watch?v=X4KivZmTM-I

## Comment utiliser Python sur sa machine ?

Et bien, il faut déjà avoir installé Python sur sa machine. Si vous utilisez Windows, vous pouvez le télécharger sur le site officiel : https://www.python.org/downloads/

Installer ce logiciel, c'est installer l'interpréteur Python sur sa machine.

Si vous utilisez MacOS ou Linux, Python doit normalement déjà être installé sur votre machine.

Avec cette seule installation. Il est donc déjà possible d'exécuter du code source écrit en Python. Vous pouvez tout à fait écrire un code Python dans n'importe quel éditeur de texte, comme le bloc-notes de base de Windows. Mais cela présenterait vite ses limites.

Généralement, lorsque l'on souhaite développer (càd écrire du code, programmer), on utilise un environnement de développement (IDE en anglais). Il s'agit d'un logiciel dont le coeur sera bien sûr un éditeur de texte, mais qui possédera beaucoup d'autres fonctionnalités bien utile pour être plus efficace et travailler plus confortablement.

Il existe plusieurs IDE, pour différents langages et ayant chacun des spécificités différentes. Dans le cadre de ce séminaire, nous utiliserons PyCharm. La version *Community* est gratuite (et *open-source*, comme Python). Lien pour télécharger : https://www.jetbrains.com/pycharm/download/


Une fois les deux logiciels installés, nous allons pouvoir commencer à écrire du code. Au premier lancement de PyCharm, ce dernier vous proposera (entre autres) de créer un nouveau projet. Un projet s'apparente simplement à un dossier dans votre ordinateur. Le logiciel vous demandera où vous souhaitez créer votre dossier (choisissez un emplacement qui vous convient sur votre machine), puis cliquez sur "créer", en laissant le reste par défaut.

À ce moment, l'éditeur de texte de PyCharm devrait s'afficher.

![Image](./pyCharmStartup.png "PyCharm et son éditeur de texte")

A gauche se trouve l'explorateur de projet. Pour l'instant, vous pouvez voir qu'il n'existe qu'un seul fichier Python dans votre projet/dossier. Il a été créé automatiquement par PyCharm et s'appelle *main.py* (*.py* étant l'extension des fichiers Python, comme *.doc* pour un fichier Word par exemple).

Au centre se trouve l'éditeur de texte. C'est là que l'on peut écrire dans ses fichiers Python.

En haut à droite, se trouve un bouton *play* en vert. Cliquez dessus. Le script *main.py* vient d'être exécutée ! Un nouvel espace est normalement apparu en bas de la fenêtre. Il s'agit de la console. Cela permet au programme exécuté d'afficher des infos, mais aussi à l'utilisateur·ice du programme d'en envoyer vers Python.

## Écrire un premier script

Pour l'instant, le code source du fichier *main.py* peut sembler un peu obscur. Nous verrons plus tard ce qu'il signifie. En attendant, vous pouvez créer un nouveau fichier Python. Pour cela, faites clic droit sur le dossier de projet en haut à gauche (appelé *pythonProject* sur la capture du dessus, mais peut-être avec un autre nom chez vous) et faites nouveau fichier -> fichier Python. Nommez le par exemple *demo1*.

Ce fichier est automatiquement ouvert dans l'éditeur. Vous pouvez voir qu'il y a maintenant deux onglets au dessus de celui-ci. Vous pouvez ainsi avoir plusieurs fichiers ouverts en même temps dans votre éditeur, et passer de l'un à l'autre aisément. Vous pouvez également constater, et l'on pouvait s'y attendre, que ce fichier est complètement vide.

Vous allez ainsi pouvoir écrire votre premier programme en python. Pour l'instant, ajoutez simplement en première ligne :

```python
print("hello world")
```

Faites clic droit sur l'onglet de *demo1.py* puis *lancer 'demo1'*

La console en bas affiche maintenant une première ligne (vous indiquant quelle fichier/script vient d'être interprété) puis le message `hello world`. Enfin, en dessous se trouve une ligne vous indiquant que l'exécution du script s'est terminé.

Le script que vous venez d'écrire est très simple : il ne contient qu'une seule instruction. En Python, la fin d'une instruction est marquée par une fin de ligne.

`print()` est ce qu'on appelle une fonction. Nous aborderons ce concept plus en détail plus tard, mais vous pouvez déjà garder en tête que cela suit plus ou moins le même principe qu'une fonction en mathématiques. Néanmoins ici, on comprend facilement qu'elle sert à afficher quelque chose dans la sortie console.

Ajoutez à la ligne 2 l'instruction suivante : 

```python
print(4+5) # ceci est un commentaire
```

puis relancez le script. Vous verrez cette fois dans la sortie, en dessous de `hello world`, le chiffre `9`. Nous venons ici de réaliser une addition de deux nombres entiers, dont le résultat est affiché par `print()`

Ce qui est placé ensuite est un commentaire. Tout ce qui est écrit après un `#` n'est pas considéré par l'interpréteur, pour lui c'est comme si ce texte n'existait pas. Cela permet donc de commenter son code.

Voici comment faire pour commenter plusieurs lignes :

```python
'''
print(4+5)
print(5+10)
print(10+50)
'''
```

le symbole `+` est un opérateur (même terme qu'en mathématiques donc). En Python, comme dans la plupart des langages, nous pouvons utiliser des opérateurs pour faire des calculs mathématiques. En voici la liste :

|symbole | fonctionnalité |
|--------|----------------|
|\+ | addition |
|\- | soustraction |
|\* | multiplication |
|\**| puissance |
|\/ | division |
|\//| division entière |
|\% | modulo |

Effectuer des calculs ligne par ligne peut sembler intéressant, mais le vrai intérêt va réellement être de pouvoir stocker ces résultats, afin de pouvoir les réutiliser plus tard dans notre script.

## Les variables

C'est à ce moment que le concept de variable entre en jeu. Une variable sert à référencer une valeur, ou plus exactement, en Python, toujours un objet.

Rappelez-vous de ce qui a été dit plus tôt : en Python, **tout est objet**. Nous allons tenter d'expliquer un peu mieux ce que cela signifie.

Toujours dans le même fichier, ajoutez à la ligne suivante :

```python
x = 5
```

Ici, x est une variable. Ce que fait cette instruction, c'est affecter à la variable `x` la valeur `5`. Contrairement aux mathématiques, le symbole `=` ne marque pas une égalité entre ce qui se trouve à sa gauche et à sa droite, mais bien le fait de venir affecter ce qui se trouve à droite à la variable de gauche (à gauche du `=` se trouve toujours une variable).

NB : convention de nommage : On écrit les variables en minuscules, en séparant les mots par un _. Exemple : `longueur`, `nb_valeurs`. Ci-dessous un lien reprenant toutes les conventions de nommage du langage : 

https://pythonguides.com/python-naming-conventions/

Il est possible de donner quasiment n'importe quel nom à une variable. Seul les mots réservés au langage, par exemple `def` ou `if` ne peuvent pas être utilisés en tant que nom de variable.

Vous pouvez maintenant ajouter les lignes suivantes : 

```python
y = 10
z = x + y
print(z)
```

Si vous lancez le script, vous verrez à la suite des autres prints le nombre entier 15. En effet, nous avons bien affecté à la variable z la somme de `x` et de `y`, à savoir 5 + 10.

Mais alors, qu'est-ce qu'un objet, et qu'est-ce qu'une variable par rapport à un objet ? Quand on écrit ` y = 10`, `y` est une variable, et `10` un objet de **type** (nous expliquerons ce terme juste après) `int` pour *integer* en anglais, soit un nombre entier.

Ce n'est pas `y` qui vaut directement `10`. En fait, la variable sert de référence vers l'objet de type `int` qui représente le nombre 10.

Prenons l'exemple d'une bibliothèque universitaire. Si je veux trouver un livre, j'ai besoin de sa référence, ou plus exactement de sa côte de rangement (par exemple : BIB. : P. du Midi 53 EX/COR KAN). Et bien cette côte, c'est l'équivalent de notre variable. Elle me permet de savoir où se trouve le livre en question, mais **N'EST PAS** le livre en question. Le livre, c'est l'équivalent de notre objet.

Pour filer davantage la métaphore, les étagères de la bibliothèque sont l'équivalent de la mémoire vive (la RAM) de notre machine. Quand un nouvel objet est créé, celui-ci est stocké dans cette mémoire vive. Et c'est la variable qui nous permet de savoir où est rangé cet objet dans la mémoire.

Maintenant, ajoutez la ligne suivante :

```python
print(4		# va engendrer une erreur
```

Puis relancez votre programme. Vous devriez obtenir un affichage similaire dans la console : 

```python
  File "/home/sylvain/HEFF/2021-2022/PYTHON/seminaire-prog-python/code/demos/demo1.py", line 11
    str1 = "hello world "   # objet String : chaine de caractères
    ^
SyntaxError: invalid syntax
```

Vous venez d'effectuer votre première erreur ! En effet, nous avons oublié de fermer la parenthèse de la fonction `print()`.

Vous pouvez constater que l'éditeur souligne en rouge une partie de votre code. Tout simplement car PyCharm a détecté qu'il y avait une erreur de syntaxe. Cette fonctionnalité fait partie des nombreux avantages d'un IDE par rapport à un éditeur de texte type bloc-notes Windows. D'ailleurs, vous avez peut-être déjà remarqué que quand vous ouvrez des parenthèses, Le symbole "`)`" est ajouté automatiquement. Une autre fonctionnalité bien pratique.

## Les types natifs

En python, chaque objet a un type. C'est à dire ce qu'il \*est\*. Il est tout à fait possible (c'est le but de la programmation orientée objet) de créer ses propres types. Mais Python fournit déjà quelques types dits natifs, qui sont utilisables directement dans n'importe quel code source.

Nous avons déjà parlé du type `int`, qui représente un nombre entier. Voici l'ensemble des types natifs :

|  Dénomination en Python | en français |
|-| -|
| int | nombre entier optimisé |
| float | nombre à virgule flottante |
| long | nombre entier de taille arbitraire (obsolète) |
| bool | booléen |
| str | chaîne de caractère |
| complex | nombre complexe |
| unicode | chaîne de caractère unicode (obsolète) |
|list | liste de longueur variable |
|tuple | liste de longueur fixe |
| dict | dictionnaire |
| file | fichier |
| NoneType | absence de type |
| NotImplementedType | absence d'implémentation |
| function | fonction |
|module | module |

Certains de ces types seront abordés plus tard. Commençons par présenter plus en détail les types "de base".

`int` : nombre entier. Rien de compliqué ici, représente un nombre entier, positif ou négatif.

`float` : nombre à virgule flottante. Idem, pas besoin de savoir ce que signifie flottante pour les utiliser. À noter qu'en Python (et dans la plupart des langages), on représente la virgule par un point : `a = 4.52`

`bool` : un booléen ne peut prendre que deux valeurs : `True` ou `False`. Nous verrons très vite leur utilité.

`str` : *String*; chaine de caractère. À vrai dire, nous avons déjà utilisé une chaine de caractère :

`str = "hello world`

ici, `"hello world"` en est une. Pour écrire une chaîne de caractère, on note celle ci entre guillemets. On peut aussi utiliser des apostrophes ou même des triples guillemets.

```python
str1 = "bonjour"
strbis = "bonjour les \"amis"
str 2 = 'hello'
str3 = """halo l'aventure "formidable" """
```

Une chaîne de caractère est une séquence. Cela signifie que l'on peut appréhender celle-ci comme une "suite" de caractères simples. Essayez d'exécuter ces instructions dans votre script :

```python
str1 = "hello world"
premier_char = str1[0]
troisieme_char = str1[2]
dernier_char = str1[-1]
print(premier_char)
print(troisieme_char)
print(dernier_char)
```

Sera alors affiché dans la console : 

```
h
l
d
```

Les instructions lignes 2, 3 et 4 viennent à chaque fois affecter un des caractères de la chaîne dans 3 variables différentes.

On appelle indice (souvent appelé `i`) l'entier placé entre crochets, permettant de récupérer le i-ième élément de la chaîne.

**IMPORTANT : en informatique, on commence toujours à compter à partir de 0.**

Utiliser un indice négatif permet de démarrer depuis la fin de la chaîne. (-2 renverra l'avant dernier caractère, et ainsi de suite).

On peut également écrire une chaîne en la notant entre apostrophes ou entre triplets :

```python
str2 = 'bonjour les "amis" '
str3 = """j'espère que vous allez bien,\n moi oui"""
print(str2 + str3)
```

Deux choses à noter : il existe des caractères "spéciaux", que l'on peut écrire dans une chaîne à l'aide du `\` (antislash ou *backslash* en anglais). Par exemple, `\n` est un caractère de saut de ligne. Un autre caractère souvent utilisé est `\t`, pour insérer une tabulation.

Dans la troisième ligne, nous utilisons le symbole `+` pour afficher les deux chaînes de caractères à la suite. On appelle cela une concaténation.

On dit que Python qu'il est un langage dynamique. Étudions ce bout de code :

```python
a = 5
a = 8.6
a = "Sylvain"
```

Ici, nous n'avons créé qu'une seule variable. On lui affecte d'abord un `int`, puis un `float` et enfin un `str`. Si vous ajoutez ce code à votre script et que vous l'exécutez, tout va fonctionner correctement. Cette caractéristique rend Python assez flexible d'utilisation. Dans d'autres langages, il faut spécifier explicitement le type d'une variable, et il serait donc impossible pour celle-ci de successivement faire référence à un entier puis à un *string*.

Le code ci-dessous présente quelques fonctions de conversion de type, qui nous seront utiles plus tard.

```python
nb = 5
nb_str = str(nb)

texte1 = "4"
a = int(texte1)
texte2 = "5.23"
b = float(texte2)
```

nb est de type `int`. la ligne en dessous de cette affection sert à convertir cet entier en une chaîne de caractère, qui sera `"5"`. Cette conversion s'effectue grâce à la fonction `str()`.

Inversement, il est possible de convertir une chaîne de composée de symboles représentant un nombre (entier ou à virgule) en un type `int` ou `float`. Pour cela, on utilise les fonctions `int()` et `float()`.

On dit que ces trois fonctions "renvoient" un objet. En effet, celles-ci sont placées à gauche de l'opérateur d'affectation (le `=`). `nb`, `a` et `b` vont donc respectivement référencer vers un objet de type `str`, `int` et `float`.

Vous souhaitez connaître le type d'un objet ? Voici comment faire :

```python
print(type(nb))
```


## La console interactive

Nous avons expliqué précédemment que Python est un langage interprété. A chaque fois que vous lancez un script, l'interpréteur va traduire "ligne par ligne" le code source pour que l'instruction puisse être comprise et exécutée par la machine.

Il est donc tout à fait possible de tester du code en direct. Pour cela, on utilise ce qu'on appelle parfois la console interactive.

Il y a plusieurs moyen d'accéder à une console interactive. Si vous êtes sous Windows, vous pouvez lancer le programme "Python 3.x" (x devrait normalement être 10 chez vous, si vous avez installé la dernière version). Une fenêtre noir devrait s'ouvrir, dans laquelle vous pouvez écrire.

Il s'agit de la console "de base" de Python, qui est présente sur votre machine justement car vous avez installé Python.

Écrivez l'instruction suivante, puis appuyez sur Entrée :

```
>>> a = 5
```

En faisant ceci, vous venez d'envoyer à l'interpréteur une instruction. A la ligne suivante, écrivez cette instruction :

```
>>> print(a)
```

La console devrait vous afficher "5" à la ligne du dessous ! Il est donc possible de tester des petites portions de code rapidement de cette manière. Cela permet d'éviter de lancer à chaque fois votre script en entier, si vous êtes en train de travailler sur un point spécifique de votre programme.

Il est également possible d'utiliser une console interactive à l'intérieur de PyCharm. Tout en bas de la fenêtre se trouve plusieurs onglets. Celui tout à droite, "Console Python" permet d'ouvrir la console interactive.

L'avantage de cette console par rapport à celle de base, c'est que l'on peut voir à tout moment, dans la partie droite, quelles sont les variables déclarées, vers quel type d'objet elles référencent, et ce que contient l'objet en lui-même.

## La logique booléenne

Nous avons évoqué précédemment le type `bool`, qui peut prendre deux valeurs : `True` ou `False`. On peut ainsi faire référencer une variable vers un booléen

```python
a, b = True, False
```

NB : cette instruction est un affectation parallèle. `a` vaudra `True`, et `b` vaudra `False`

Utiliser une variable pouvant être `True` ou `False` peut présenter un intérêt. Mais l'intérêt principal des booléens est de pouvoir indiquer le résultat d'une **comparaison**, qui s'effectuent à l'aide de comparateurs :

| symbole | signification |
| - | - |
| < | strictement inférieur |
| > | strictement supérieur |
| <= | inférieur ou égal |
| >= | supérieur ou égal |
| == | égal |
| != | différent |
| x is y | x et y représentent le même objet |
| x is not y | x et y ne représentent pas le même objet |

Testez maintenant ces lignes de code dans votre script :

```python
a = 5 < 6
print(a)
```

La console affiche `True`

Les opérateurs logiques se retrouvent souvent utilisés avec les opérateurs logiques :

| symbole | signification |
| - | - |
| x or y | OU booléen|
| x and y | ET booléen |
| not x | NON booléen |

Ajoutez les lignes suivantes à votre script : 

```python
a = 5 < 6 and 10 < 6
print(a)
```

Cela va afficher `False`. En effet, on comprend intuitivement que ET signifie que les deux *conditions* (celle à gauche et celle à droite du `and`) doivent être vraies.

Si vous remplacez le `and` par un `or`, alors le print affichera cette fois `True`. Car il suffit qu'une des deux conditions soient vraies pour que l'ensemble renvoie `True`.

Voyons maintenant quel intérêt concret tout cela a.

## Les structures conditionnelles : `if`, `elif`, `else`

La plupart du temps, lorsque l'on écrit un programme, celui-ci doit se comporter différemment en fonction de différentes conditions. C'est là qu'interviennent les structures conditionnelles.

Créez un nouveau fichier Python, appelé "demo_if". Ajoutez les lignes suivantes :

```python
a = 5
if a == 5:
	print("a est égal à 5 !")
```

Lancez ce script. La console affiche : `a est égal à 5 !`

Voici notre première structure conditionnelle, très simple. La première ligne affecte à la variable `a` la valeur 5
En dessous se trouve la structure en elle-même. Le mot clef `if` ("si" en français) marque le début d'un bloc d'instruction. Le symbole `:` est également obligatoire pour marquer le début de ce bloc.

En effet, si a est égal à 5 (condition que l'on vérifie en utilisant l'opérateur `==`), alors l'opération renvoie `True`, et le code placé dans ce bloc sera exécuté. Soit ici, la ligne avec la fonction `print()`.

En python, pour placer des instructions dans un bloc, il faut indenter la ligne (avec 4 espaces, ou une tabulation). Vous voyez qu'ici, le `print()` n'est pas au même niveau que les lignes du dessus, mais décalé "une fois" vers la droite.

Changez la valeur de `a` (n'importe laquelle sauf 5 donc) dans votre code, et ajoutez la ligne suivante en dessous, sans l'indenter :

```python
print("print en dehors du bloc")
```

La console affichera uniquement le message `print en dehors du bloc`. En effet, cette fois, la condition `a == 5` n'est pas vérifiée. L'opération renvoie donc `False`, et l'instruction `print("a est égal à 5")` n'est pas exécutée.

Par contre, la ligne suivante, qui ne fait pas partie de la structure conditionnelle, est bien exécutée.

Pour réaliser des structures conditionnelles avec plusieurs "chemins" possibles, on utilise les mots clefs `elif` (pour *else if*, "sinon si" en français) et `else` ("sinon" en français).

Ajoutez ce code à la suite de votre script : 

```python
b = 10
if b == 10:
	print("b vaut 10")
elif b == 15:
	print("b vaut 15")
else:
	print("b ne vaut ni 10 ni 15")
```

La console affiche `b vaut 10`. Testez le programme en changeant la valeur de `b`.

En mettant `b = 15`, alors c'est la deuxième condition (celle du `elif`) qui est vérifiée. le message affichée sera donc `b vaut 15`. Si vous fixez `b` à n'importe quelle autre valeur, alors c'est l'instruction du bloc `else` qui est exécutée.

Maintenant, ajoutez le code suivant : 

```python
c = 20
if c > 10:
	print("c est strictement supérieur à 10")
elif c > 5:
	print("c est strictement supérieur à 5")
```

Ce bout de code va faire afficher `c est strictement supérieur à 10`, mais pas `c est strictement supérieur à 5`. Car dans une structure conditionnelle `if..elif..else`, l'ordre à son importance. Si la première condition est vérifiée, alors toutes celles qui viennent ensuite ne sont pas analysées, et les instructions des blocs suivants ne seront pas exécutées.

Une structure conditionnelle commence donc toujours par un bloc `if`, suivi de zéro, un ou plusieurs bloc(s) `elif` pour se terminer (mais pas obligatoirement) par un bloc `else`.

Si on remplace le `elif` par un `if` dans le bout de code ci-dessus, alors on obtient deux structures conditionnelles indépendantes. La console affichera cette fois : 

```python
c est strictement supérieur à 10
c est strictement supérieur à 5
```

Notez enfin, qu'il est tout à fait possible d'imbriquer un bloc de code dans un autre. Il est donc possible d'avoir une structure conditionnelle dans un bloc d'une structure. Exemple :

```python
if a > 5:
	print("a > 5")
	if a > 10:
		print ("a > 10")
```

On repère facilement les différents "niveaux" dans le code grâce à l'indentation, qu'il faut donc toujours mettre en place correctement.

## Exercice : thermomètre

Écrivons un programme simple en guise de premier exercice. l'objectif de ce script est d'afficher un message en fonction d'une température. Nommons le "thermometre_v1". L'objectif est le suivant :

- Si la température est négative, afficher "La température est négative"
- Si la température est positive, afficher "La température est positive"
- Si la température est nulle, afficher "Il fait 0°C"
- Si la température est inférieure à -30°C, afficher "Il fait très froid !"
- Si la température est supérieure à 40°C, afficher "Il fait très chaud !"

Le programme doit, dans tous les cas, n'afficher qu'une seule phrase.

La première ligne de votre programme sera :

```python
temperature = 15
```

À vous de remplacer ce 15 par une autre valeur afin de vérifier que votre programme détecte bien les 5 cas possibles.

Essayons maintenant d'améliorer ce programme, pour qu'il affiche dans tous les cas si la température est positive,négative ou nulle, et qu'il affiche, si c'est bien le cas, s'il fait très chaud ou très froid.

Essayer de résoudre ce problème en utilisant plusieurs méthodes : Soit en utilisant plusieurs structures conditionnelles indépendantes, soit en utilisant des conditions écrites avec les opérateurs `and` et/ou `or`, soit en écrivant des structures imbriquées (`if` dans des `if`).

Afin de mieux vous y retrouver, je vous conseille, pour chaque version différente de ce programme, de créer à chaque fois un nouveau fichier Python. (thermometre_v2, et ainsi de suite).

Vous êtes bloqué·es ? Vous pouvez consulter les solutions dans le dossier /code/exercices/


## La boucle `while`

Dans la plupart des programmes informatiques, il est nécessaire d'effectuer plusieurs fois la même action, donc effectuer quelque chose "en boucle". On parle également d'itérations, et donc de boucle itérative.

Une des deux manières de faire en Python est la boucle `while` ("tant que" en français). Comme son nom le laisse supposer, elle sert à effectuer une action tant que sa condition d'entrée est `True`.

Unne boucle `while`, comme pour les `if`, `elif` et `else`, s'écrit en tant que bloc d'instructions :

```python
n = 0
while n < 10:
	print("n = " + str(n))
	n += 1	# affection et modif en 1 instruction : revient à faire n = n + 1
print("Programme sorti de la boucle while)
```

Créez un nouveau script Python appelé "demo-while.py". Écrivez-y le code ci-dessus et exécutez le. Vous devriez obtenir la chose suivante dans la console :

```
n = 0
n = 1
n = 2
n = 3
n = 4
n = 5
n = 6
n = 7
n = 8
n = 9
Programme sorti de la boucle while
```

Les instructions à l'intérieur de la boucle ont été exécutées 10 fois. En effet, lorsque l'interpréteur arrive pour la première fois à la ligne 2 du code, la condition `n < 10` est `True` (car `n` vaut 0). Le programme "rentre" dans la boucle et exécute les deux lignes.

NB : la deuxième ligne de la boucle contient un opérateur d'affectation particulier : il sert à "mettre à jour" la valeur référencée par la variable `n`. À la fin de cette première itération, `n` vaut 1.

Là, le programme retourne au début de la boucle, et vérifie à nouveau si la condition est `True`. C'est toujours le cas, donc les instructions de la boucle sont à nouveau exécutées. Et ainsi de suite, 10 fois.

À la fin de la dixième itération, `n` vaut 10. Par conséquent, quand le programme retourne vérifier la condition d'entrée de la boucle, celle-ci renvoie `False`, car `n` n'est pas strictement inférieur à 10. Le programme ne rentre donc pas dans la boucle, et va continuer en exécutant l'instruction suivante, en l'occurrence `print("Programme sorti de la boucle while)`.

Avant de passer à l'exercice suivant, nous allons présenter une autre fonction de base. Nous avons déjà beaucoup utilisé la fonction `print()` servant à afficher un message dans la console. Mais il est également possible d'utiliser celle-ci pour envoyer des chaînes de caractère au programme. Pour cela, on utilise la fonction `input()`

```python
nom = input("Entrez votre nom : ")
print("Bonjour " + nom)
```

Exécutez ce bout de code. Contrairement à tous les programmes exécutes jusqu'ici, la console n'affiche pas `Process finished with exit code 0`. C'est car la console attend que l'utilisateur·ice du programme entre du texte au clavier. On appelle cela une fonction bloquante.

NB : pour interrompre l'exécution d'un programme (qui serait bloqué dans une boucle "infinie"), appuyez sur le logo "stop" en rouge à gauche du terminal.

Cliquez sur la console et entrez votre nom. Celui-ci apparaît en vert. Appuyez sur Entrée.

La console vous dit bonjour ! En effet, grâce à la fonction `input` vous venez de créer un objet à partir de ce que vous avez tapé au clavier, objet qui sera référencé par la variable `nom`. Comme pour les fonctions `int()` ou `str()` vues précédemment, la fonction `input()` renvoie une référence vers un objet.

## Exercice : le juste prix

Le but de cette exercice est de réaliser un programme reprenant le principe du juste prix. Vous devez deviner le prix d'un bien quelconque. Le programme vous demandera de donner un prix, et vous répondra en disant si le prix du bien est plus grand ou plus petit que le prix entré. Le programme continuera de vous demander un prix **tant que** vous n'avez pas donné le juste prix.

Créez un nouveau fichier Python, que vous pouvez appeler "justeprix". Le programme commencera par les deux lignes suivantes :

```python
prix = 450	#prix à deviner, vous pouvez mettre n'importe quel nombre
proposition = int(input("Entrez une proposition : "))
```

NB : la deuxième instruction peut vous sembler étrange. Il n'en est rien. Elle appelle simplement deux fonctions successivement.
D'abord, le programme va exécuter la fonction `input()`. Le message va s'afficher, et le programme va attendre que vous rentriez une chaîne de caractère. Cette chaîne est renvoyée par la fonction. C'est une chaîne, donc de type `str`. Or, il faut ensuite convertir la chaîne en type `int` afin de pouvoir comparer avec la valeur de `prix`.

Si jamais ce n'est pas clair, voici du code qui fait la même chose, mais en deux instructions :

```python
chaine = input("Entrez une proposition : ")
proposition = int(chaine)
```

Indice : vous aviez peut-être déjà deviné en lisant l'énoncé, mais il faudra utiliser une structure conditionnelle à l'intérieur de la boucle.

## Les listes et les tuples

Présentons maintenant ces deux types natifs du langage : les listes et les tuples.

Ils s'agit d'objets séquentiels, comme les chaîne de caractère. Pour créer une liste, on peut écrire :

```python
li = [1, 4.5, "bonjour", True, 785, "hello"]
```

Et pour créer un tuple :

```python
tu = (1, 4.5, "bonjour", True, 785, "hello")
```

Vous pouvez ajouter ces deux lignes dans un fichier Python "demo_list_tuple".

Comme vous pouvez le constater, une liste/ un tuple peuvent contenir tout type d'objets

Ces deux séquences contiennent les 3 mêmes éléments. Quel est donc la différence entre une liste et un tuple ?

Et bien, une liste est mutable, alors qu'un tuple est immuable. Mutable signifie pouvant être modifié après création. Il est possible d'enlever et d'ajouter des éléments à une liste. Un objet immuable est à l'inverse non modifiable. Une fois un tuple créé, impossible d'y ajouter ou retirer des éléments. Les chaînes de caractère sont également immuables.

Quel est l'intérêt d'avoir ces deux types en Python : pour la liste, cela parait évident. La possibilité d'avoir une séquence pouvant être mise à jour est sans équivoque. Dans quel cas utiliser un tuple ? Lorsque l'on souhaite de meilleures performances. Récupérer un élément d'un tuple s'effectue plus rapidement que pour une liste.

Alors, comment récupérer un élément ? Et bien, comme pour une chaîne de caractère, en écrivant le nom de la variable référençant vers la liste/le tuple directement suivi de l'indice entre crochets :

```python
print(li[0])
print(tu[2])
```

Le premier print affichera `1`, le second affichera `bonjour`.

On peut accéder à une partie de la séquence comme ceci :

```python
debut_li = li[0:2]
print(debut_li)
```

Attention : la borne supérieure est excluante. La variable `debut_li` référence donc vers une nouvelle liste contenant uniquement l'élément 0 et l'élément 1 de la liste `li`, mais pas l'élément 2.


Comment connaître le nombre d'éléments d'une séquence :

```python
nb_elts = len(li)
print(nb_elts)
```

Ce code affichera `3`

Comment modifier un élément dans une liste :

```python
li[0] = 45
```

Comment ajouter un élément à la fin d'une liste :

```python
li.append("hello")
```

Comment supprimer un élément d'une liste : 

```python
li.remove(4.5)		# supression si l'on connait l'élément
li.remove(li[0])		# supression d'un élément dont on connait l'indice
```


Vous pouvez tester le code suivant dans votre script :

```python
li[0] = 45
li.append("hello")
print(li)
li.remove(4.5)
li.remove(li[0])
print(li)
```

Vous pourrez ainsi constater que la modification l'ajout et le retrait des éléments ont bien fonctionné.

Nous venons, à travers ces exemples, de voir la syntaxe pour utiliser des fonctions opérant sur des objets : `variable.fonction()`. On appelle ce genre de fonctions des méthodes. Nous aborderons d'avantage ce point dans la partie programmation orientée objet.

Il existe quantité de méthodes pour tous les types natifs de Python. Si vous souhaitez consulter la documentation d'un type donné, vous pouvez les faire en ouvrant la console interactive, et écrire :

```python
help(list)
```

La console vous affichera les différentes méthodes du type entré entre les parenthèses, avec ce qu'elles font, ce qu'elles prennent en entrée et ce qu'elles renvoient. Si vous préférez, il est tout à fait possible de trouver cette documentation sur Internet. N'hésitez pas à la consulter pour "découvrir" les différentes méthodes existantes.

Autre *tips* utile, à propos de PyCharm. Dans l'éditeur de code, vous pouvez facilement visualiser la doc d'une fonction/méthode. Placez votre curseur sur la fonction en question, et faites le raccourci clavier -ctrl+q-. la documentation relative à la fonction/méthode en question s'affichera.

## La boucle `for`

La boucle `for` est le deuxième type de boucle qu'on retrouve dans les langages de programmation. En python, elle est utilisée pour parcourir des séquences.

Créez un fichier "demo_for.py", et ajoutez-y les lignes de code suivantes :

```python
langages = ["C", "C++", "Java", "Python", "HTLM", "CSS", "PHP", "Go", "Rust"]
for langage in langages:
	print(langage)
```

En exécutant ce script, on peut afficher successivement tous les éléments de la liste `langages`. La variable `langage` prend à chaque nouvelle itération de la boucle la valeur suivante de la liste.

Il est aussi possible de récupérer en même temps l'élément et l'indice correspondant :

```python
for i, langage in enumerate(langages):
	print(str(i) + " : " + langage)
```

La fonction `range(inf, sup, pas)` est aussi beaucoup utilisé pour générer rapidement une séquence de nombres. NB : le pas est optionnel

```python
for i in range(0, 10, 2):
	print(i)
```

Ce bout de code va afficher les entiers de 0 à 9 dans la console.

Avant de travailler sur le prochain exercice, parlons de deux mots clefs utilisés dans les boucles `for` et `while` : `break` et `continue`.

Le premier permet de sortir directement d'une boucle. C'est à dire sans terminer l'itération en cours ni retourner vérifier la condition d'entrée. Le second permet de retourner directement au début de la boucle. Dit autrement, cela permet de terminer directement l'itération en cours. Leur utilisation peut être utile dans certains cas, mais il est quasiment toujours possible de faire la même chose uniquement avec des structures conditionnelles.

Car en informatique, il n'y a rarement qu'une seule solution possible pour résoudre un problème donné. Peut-être que, pour un exercice quelconque, vous allez écrire un code source qui va fonctionner correctement, mais qui sera différent du mien.

## Exercice : Trier des légumes

Mise en situation : vous êtes un·e maraîcher·ère qui cultive deux légumes différents : des chicons et des patates. Vous possédez dans votre entrepôt deux réserves pour ranger les légumes séparément. Ces réserves seront symbolisées par deux listes :

```python
res_chicons = ["chicon", "patate", "patate", "chicon", "chicon", "patate"]
res_patates = ["chicon", "chicon", "chicon", "patate", "patate", "chicon", "patate", "chicon"]
```

La suite de votre script doit ranger les légumes afin qu'ils soient tous dans la bonne réserve. À la fin, vos listes devraient apparaître ainsi dans la console :

```
["chicon", "chicon", "chicon", "chicon", "chicon", "chicon", "chicon", "chicon]
["patate", "patate", "patate", "patate", "patate", "patate"]
```

Deux corrigés sont disponible dans le dossier /exercices. Celui nommé "trilegumes.py" devrait ressembler plus ou moins à ce que vous avez fait. Le second est une version sans boucle ! Ce script met particulièrement à profit différentes méthodes du type `list`.

Et en plus, elle est plus simple à comprendre ! N'hésitez donc pas à lire la doc Python, et à chercher à l'aide de votre moteur de recherche favori comment faire quelque chose. Par contre, il vaudra vraiment mieux faire sa recherche en anglais.

## Les dictionnaires

Un dictionnaire est un type composite (au même titre que les listes ou les tuples par exemple), mais n'est pas un séquence.

Pour en expliquer son principe, il suffit de faire l'analogie avec un dictionnaire "dans la vraie vie". Chaque mot du dictionnaire possède une définition, et bien en Python, on dit qu'à chaque clef (*keys* en anglais) du dictionnaire correspond une valeur (*value* en anglais).

Un dictionnaire Python ne peux pas contenir deux clefs identiques, comme dictionnaire ne peux pas contenir deux fois le même mot.

Un dictionnaire Python est donc un ensemble de paires clef: valeur.

A la différence d'une liste ou d'un tuple, l'ordre de ces éléments (les paires) n'est pas immuable. C'est pour cela que le dictionnaire n'est pas une séquence.

Voici comment on peut créer un dictionnaire :

```python
dico = {6: 4.5, "bonjour": [4, "hello"], 75.23: 4.5, (1, 2, 3): 10}
```

On peut placer tout type d'objet en tant que valeur. En revanche, la clef doit être un objet *hashable*. Pour faire simple, retenez qu'un objet *hashable* est immuable. Donc, impossible par exemple d'avoir une liste en tant que clef.

En pratique, on utilise très souvent uniquement des chaînes de caractère pour les clefs.

Voici comment récupérer la valeur correspondante à une clef :

```python
a = dico[6]
print(a)
```

On affiche ainsi `4.5` dans la console. Le principe est le même que pour un *string*, un tuple ou une liste. Sauf qu'à la place d'écrire un indice entre les crochets, on écrit la clef.

Voici comment ajouter une paire clef: valeur :

```python
dico["hello"] = 55
```

Si on écrit une clef déjà existante entre les crochets, alors on modifie la valeur correspondante.

Voici comment supprimer un élément :

```python
del dico["bonjour"]
```

`del` est un autre mot clef du langage Python. Il sert à supprimer un objet. L'instruction ci-dessus supprime donc la liste `[4, "hello"]`. Si on supprime la valeur d'une clef, la clef est également retirée du dictionnaire.

NB : on peut utiliser `del` pour supprimer n'importe quel objet.

Comme pour les listes, il existes plusieurs méthodes pour travailler avec des dictionnaires. En voici quelques-unes :

- `keys()` : renvoie les clefs d'un dictionnaire
- `values()` : renvoie les valeurs
- `items()` : renvoie les clefs et les valeurs, sous forme de tuples

À vous de lire la doc pour connaître les autres méthodes.

## Exercice : gestionnaire de *login*

L'objectif ici est de réaliser un programme de gestionnaire de *login*. C'est à dire une paire `username`; `password`. Ces *logins* devront être stockés dans un dictionnaire.

Le programme donnera la possibilité à l'utilisateur·ice du programme de créer un compte, d'en supprimer un et de changer un mot de passe.

Comme pour un vrai site :

- il ne peut pas y avoir plusieurs comptes avec le même *username*
- Il faut demander le mot de passe et vérifier qu'il est bon pour supprimer un compte
- Idem pour changer un mot de passe, il faut d'abord donner l'ancien pour pouvoir entrer le nouveau

Votre programme devra faire tourner une boucle infinie. Il suffit pour cela de faire `while True:`. Code de départ :

```python
while True:
    choix = input("Selectionnez une action :\n1 : créer un compte\n2 : supprimer un compte"
          "\n3 : changer de mot de passe\nq : quitter")
    if choix == "q":
        quit()

    #TODO
```

NB : la fonction `quit()` permet de terminer l'exécution d'un programme.

## Les fonctions

Nous avons déjà utilisé plusieurs fonctions de base du langage Python. Dans cette partie, nous allons voir comment écrire nos propres fonctions.

Le concept de fonction en informatique est très similaire à celui en mathématiques.

Une fonction informatique peut prendre quelque chose en entrée, et va renvoyer quelque chose en sortie.

Par exemple, la fonction `input()` prend en argument d'entrée une chaîne de caractère pour l'afficher dans la console, et renvoie la chaîne de caractère rentrée dans la console par l'utilisateur·ice.

Une fonction peut ne prendre aucun argument en entrée. Également, une fonction peut ne "rien" renvoyer. En Python, cette affirmation n'est pas totalement correcte. Si une fonction ne renvoie rien, elle renvoie en pratique un objet de type `None`.

Voici la syntaxe pour écrire une fonction :

```python
def print_bonjour():
	print("bonjour")
```

Cette fonction ne possède aucun paramètre d'entrée, et ne "renvoie rien" (donc `None`).

NB : La convention de nommage est la même que pour les variables : tout en minuscules, avec les mots séparés par un _.

Créez un nouveau fichier et ajoutez écrivez cette fonction au début. En-dessous de celle-ci, ajoutez ces lignes pour appeler votre fonction :

```python
a = print_bonjour()
print(type(a))
```

La console affiche bien `bonjour`, puis affiche le type de l'objet référencé par la variable `a` (`NoneType`).

Écrivons maintenant une fonction effectuant une addition de deux nombres :

```python3
def addition(a, b):
	return a + b
```

Cette fonction possède deux paramètres, et renverra la somme de ces deux objets via le mot clef `return`.

Écrivez cette fonction en dessous de la première, et ajoutez les lignes suivantes après `print(type(a))` :

```python
somme = addition(4, 8)
print(somme)
```

Cette première ligne fait passer les entiers 4 et 8 en argument aux paramètres `a` et `b`.

NB : il ne faut pas confondre paramètre et argument. Un paramètre est le nom que l'on donne à la variable d'entrée lors de la définition de la fonction (ici, `a` et `b` sont les arguments de la fonction `addition`). Les arguments sont les objets qui sont passés aux arguments de la fonction lors de l'appel de celle-ci (ici,n l'argument 4 est passé au paramètre `a`, et l'argument 8 au paramètre `b`).

La console affiche `12`. L'objet renvoyé est bien référencé par la variable `somme`

Il est possible de définir des paramètres facultatifs. Exemple :

```python
def puissance(x, y=2):
	return x**y
```

Ici, `y` est un paramètre facultatif. Testons notre fonction :

```python
b = puissance(2, 4)
c = puissance(4)
print(b)
print(c)
```

La première ligne passe l'argument 2 au paramètre `x` et l'argument 4 au paramètre `y`. La valeur par défaut de `y` (2) est écrasée.

La deuxième ligne passe simplement l'argument 4 au paramètre `x`. Lors de l'exécution de la fonction, `y` vaudra donc sa valeur par défaut, à savoir 2.

Si l'on ne connaît pas l'ordre des paramètres d'une fonction, il est possible de spécifier explicitement vers quel paramètre on passe tel ou tel argument :

```python
d = puissance(y=3, x=4)
print(d)
```

Lors de la définition d'une fonction, il est également possible d'indiquer quel type on attend de recevoir pour un argument :

```python
def compter_a(chaine: str):
	nb = chaine.count("a")
	return nb
```

Appelons cette fonction :

```python
nb_a = compter_a("aloha miami")
print(nb_a)
#nb_a = compter_a(2)
```

Relancez votre programme. la console affiche bien `3`. Maintenant, dé-commentez la troisième ligne. Vous pouvez constater que PyCharm souligne le `2` passé en argument, car PyCharm détecte que la fonction `compter_a()` est censé prendre un type `str` en argument, et non un `int`. Si vous relancez votre programme. Vous allez avoir une erreur.

Cet exemple va nous permettre d'aborder le concept de variable locale. Dans la fonction existe une variable `nb`. Et bien, il s'agit d'une variable locale. Cela signifie n'existe que dans la fonction en question.

Plus généralement, une variable n'existe que dans le bloc dans lequel elle a été déclarée. Prenons l'exemple d'une boucle `while` :

```python
i = 0
while i < 100:
    j = i ** 3
    if j % 2 == 0:
        k = j // 5
        print("j est pair")
    print(k)
    i += 1

print(j)
```

La variable j n'existe quand dans la boucle `while`. La variable `k` n'existe que dans le bloc conditionnel `if`. L'instruction `print(k)` générera une erreur, de même que l'instruction `print(j)`.

Le *scope* d'une variable désigne l'espace dans lequel celle-ci existe.

À l'inverse, il existe ce qu'on appelle des variables globales. Ajoutez au début de votre fichier les lignes suivantes :

```python
z = 42
msg = "message de base"
```

Ces variables, définies avant les fonctions, sont ainsi utilisables dans celles-ci.

**ATTENTION : il est très peu recommandé d'utiliser des variables globales.**

Il est possible de placer plusieurs `return` dans une fonction. À partir du moment où un de ceux-ci est atteint, l'exécution de la fonction se termine. On peut d'ailleurs utiliser un `return` sans rien renvoyer explicitement. Exemple :

```python
def deviner_nb():
    print("Vous avez 10 essais pour deviner le nombre secret")
    essais = 10
    guess = int(input("Entrez votre premier guess : "))
    while essais > 0:
        if guess == 13:
            print("Bravo, vous avez deviné le nombre secret !")
            return
        else:
            essais -= 1
            if essais == 0:
                print("C'est perdu")
                return
            else:
                guess = int(input("Plus que " + str(essais) + " tentatives. Entrez une nouvelle proposition : "))
```

Les fonctions présentent un intérêt fondamental : éviter d'avoir plusieurs fois le même bout de code dans son programme. En informatique, on cherche toujours à "compartimenter" son code afin d'éviter ces répétitions inutiles.


## Exercice : gestionnaire de *login* v2

Normalement, votre première version du gestionnaire de *login* doit comporter plusieurs fois les mêmes portions de code. Essayez, à partir de cette v1, d'écrire une v2 qui met à profit le concept de fonctions.

Comment démarrer :

```python
def isUsername(logins: dict):
	'''
	TODO fonction vérifiant si un pseudo est déjà dans le dictionnaire
	Renvoyer False si le pseudo n'existe pas, et renvoyer le pseudo sinon.
	'''
	pass
```

NB : le mot clef `pass` représente une instruction qui "ne fait rien"

Ici, la fonction n'a pas encore de corps ( = de code à l'intérieur). Pour quand même laisser cette fonction inachevée dans le script et pouvoir le lancer malgré tout, on utilise le mot clef `pass`
	

## Les modules

En Python, un module désigne un fichier Python contenant des fonctions. On peut voir un module comme une "boit à outils".

Python possède plusieurs modules natifs. Pour utiliser les fonctions d'un module dans un script, il faut l'importer, en début de fichier :

```python
import math
from random import *
from datetime import date
```

La première ligne sert à importer l'intégralité du module `math`. La deuxième importe également toutes les fonctions du module `time`. La troisième ligne importe uniquement la fonction `date` du module `datetime`.

Voici comment utiliser les fonctions de ces modules :

```python
y = math.sqrt(100)
print(y)

j = uniform(10, 100)
print(j)

jour = date(day=4, month=12, year=2021)
print(jour)
```

NB : il vaut mieux éviter d'importer tout un module via `from module importe *`. En effet, si l'on utilise beaucoup de modules dans un programme, il peut y avoir des conflits de nommages, car certains modules peuvent avoir des fonctions avec le même nom. Privilégiez donc `import module`, afin d'écrire explicitement dans votre code le nom du module, suivi du nom de la fonction : `module.fonction()`.

Certains modules sont utilisés avec un alias. Par exemple :

```python
import random as rand

nb = rand.randint()
```

Avant de passer à l'exercice suivant, abordons la notion d'attribut.

Nous avons expliqué ce qu'est une méthode : une fonction "faisant partie" d'un objet (par exemple `liste.append()`). Pour faire court, un attribut est un objet appartenant à un autre objet.

Par exemple, pour récupérer le jour d'une date :

```python
d = date.datetime(2021, 12, 4) # d référence vers un objet de type date.datetime
jour = d.day # day est un attribut de l'objet référencé par d. la variable jour référencera vers sa valeur, ici 4
```

Comme pour les méthodes, nous aborderons plus en détail les attributs dans la partie programmation orientée objet

## Exercice : simuler le cycle jour/nuit

L'objectif de cette exercice est de simuler le cycle jour/nuit pour un jour donné.

Afin de simplifier l'exercice, le programme pourra afficher quatre cycles différents : un pour chaque saison.

- Été (Juin, Juillet, Août) : le soleil se lève à 7h et se couche à 21h
- Automne (Septembre, Octobre, Novembre) : le soleil se lève à 9h et se couche à 19h
- Hiver (Décembre, Janvier, Février) : le soleil se lève à 10h et se couche à 17h
- Printemps (Mars, Avril, Mai) : le soleil se lève à 8h et se couche à 20h

Pour simuler le cycle jour nuit dans la console, il faudra introduire un délai. Pour cela, il faut utiliser la fonction `sleep(secondes)` du module `time` qui permet de "bloquer" le programme pendant x secondes.

La date du jour doit être rentré dans la console par l'utilisateur·ice. Il à partir de ses valeurs créer un objet de type `datetime.date`.

## Les fichiers

Nous allons voir dans cette partie comment lire et écrire dans un fichier.

L'intérêt est évident : travailler avec des fichiers en lecture et en écriture permet à la fois de travailler avec des données existantes, et d'en sauvegarder "en dur" dans un fichier.

En Python, il existe deux méthodes pour travailler avec des fichiers. Voyons la première, en mode lecture :

```python
p = open("data.txt")   #on ouvre le ficher
data = fp.readlines()   #on stocke les lignes du fichier dans la variable data : data sera une liste ayant autant d'éléments qu'il y a de lignes dans le fichier
print(type(data))
print("file contains : {}".format(data)) #print formaté : permet d'afficher simplement plusieurs données via un seul print
fp.close()              #toujours fermer le fichier si on l'a ouvert plus tot
```

Évidemment, le fichier `data.txt` doit déjà exister dans votre répertoire.

Voici la seconde méthode, à privilégier :

```python
with open("data.txt", "r") as fp:  #ici "r" signifie qu'on accède au fichier en mode lecture (read en anglais)
    data = fp.read()            #lecture simple : on récupère tout le fichier en un seul String
    print(type(data))
    print("file contains : {}".format(data))
```

Voyons maintenant comment écrire dans un fichier :

```python
note_eco = int(input("Entrez votre note en éco : "))
note_maths = int(input("Entrez votre note en maths : "))
note_anglais = int(input("Entrez votre note en anglais : "))

releve = {"note éco": note_eco, "note maths": note_maths, "note anglais": note_anglais}

bulletin = "bulletin.txt"

with open("bulletin.txt", "a") as fp:        #ouverture du fichier en mode écriture (write en anglais), + signifie créer le fichier s'il n'existe pas
    for matiere, note in releve.items():
        fp.write("{} : {}\n".format(matiere, note)) #écriture du nom de la matière suivi de la note, à chaque fois dans une nouvelle ligne

moyenne = (note_eco + note_maths + note_anglais)/3

with open(bulletin, "a") as fp:             # "a" signifie append, c'est à dire ajouter à la suite du fichier (comme la méthode append pour une liste)
    fp.write("moyenne : {}\n".format(moyenne))
```

Le second argument passé à la fonction `open()` correspond au mode d'accès.

`r` signifie lecture, `w` écriture (avec placement du curseur d'écriture au début du fichier, donc à éviter, car le contenu éventuellement existant de la fichier sera effacé) et `a` pour *append* (le curseur est placé à la fin du fichier, donc ce qui y sera écrit sera placé à la suite du contenu déjà existant). Vous pouvez ajouter un `+` à la suite de la première lettre pour accéder à la fois en lecture et en écriture au fichier.

En mode écriture, si le fichier n'existe pas, il sera automatiquement créé. Lorsque vous allez lancer pour la première fois ce script, le fichier `bulletin.txt` sera ainsi créé dans le répertoire.

## Exercice : gestionnaire de *login* v3

Nouvelle version du gestionnaire de *login* : ajoutez les fonctionnalités de sauvegarde des *login* dans un fichier texte. Le programme devra lire les *login* déjà existants dans ledit fichier au démarrage, et mettre à jour ce même fichier avant d'arrêter.

## La POO : les classes

POO = Programmation Orientée Objet.

En Python, **tout est objet**. Nous n'avons pour l'instant utilisé uniquement des types d'objets inclus dans Python de base.

Nous allons maintenant voir comment créer nos propres objets. Cela se fait en définissant une classe :

```python
class Voiture:  # définit le nom de la classe. Tout le contenu de la classe doit etre dans le bloc
    nb_voitures = 0  # attribut de classe

    def __init__(self, nb_r, nb_c):  # Méthode : fonction d'une classe
        self.nb_roues = nb_r  # attribut d'instance
        self.nb_chevaux = nb_c
        self.demarre = False
        Voiture.nb_voitures += 1  # ici on modifie l'attribut de classe

    def __str__(self):
        return f"la voiture a {self.nb_roues} roues et {self.nb_chevaux} chevaux"

    def demarrer(self):
        self.demarre = True

    def afficher_nb_roues(self):  # métohde d'instance : self en paramètre
        print(f"la voiture a {self.nb_roues} roues")

    def afficher_nb_voitures(cls):  # méthode de classe : cls en paramètre
        print(f"{cls.nb_voitures} voitures ont été crées")  # nb_voitures est bien un attribut de la classe Voiture
        # print(f"{cls.nb_roues} voitures ont été crées") #erreur : nb_roues est un attribut d'instance et non de classe

    def convertir_miles_en_km(miles):  # méthode statique
        return miles * 1.609344
```

En Python, on peut écrire une classe en utilisant le mot clef `class`. Il faut faire attention à ne pas confondre les termes classe et objet. Une classe définit les caractéristiques et les fonctionnalités d'un objet. Dans l'exemple ci-dessus, la classe `Voiture` permet de créer des objets de type `Voiture`.

NB : convention de nommage : On écrit tous les mots d'une classe avec une majuscule au début, sans _ entre les mots. Exemple : `Voiture`, `LegumeVert`

Ces objets pourront avoir des caractéristiques différentes (nombre de roues, nombre de chevaux) mais auront tous ces mêmes caractéristiques. On peut faire l'analogie avec l'espèce humaine.

Nous avons tous·te·s le même ADN, ce qui fait que nous appartenons tous·te·s à la même espèce. En revanche, nous avons tous·te·s des variations dans nos gênes qui font que nous sommes différent·e·s.

L'ADN serait donc une classe, et nos ADN avec nos gênes respectifs seraient des objets de type ADN.

Revenons à notre exemple. Ici, nous avons défini une classe `Voiture`. Pour instancier ("créer") des objets de type `voiture` écrivez le code suivant en dessous de la classe :

```python
voiture1 = Voiture(4, 4)
voiture2 = Voiture(6, 16)
```

Ces deux instructions appellent une méthode (rappel : on appelle méthode une fonction d'une classe), bien spécifique, qu'on appelle constructeur. Il s'agit de la méthode `__init__()` :

```python
def __init__(self, nb_r, nb_c):  # Méthode : fonction d'une classe
        self.nb_roues = nb_r  # attribut d'instance
        self.nb_chevaux = nb_c
        self.demarre = False
        Voiture.nb_voitures += 1  # ici on modifie l'attribut de classe
```

Dans cet exemple, le constructeur possède trois paramètres. Le premier est nommé `self`. Il représente l'objet en lui-même.

Les trois premières lignes de la méthode définissent ce qu'on appelle les attributs d'instance. Chaque objet de type `Voiture` possédera ces attributs.

Une méthode d'instance est une méthode qui sert à opérer sur une instance d'un objet. Par exemple :

```python
def afficher_nb_roues(self):  # métohde d'instance : self en paramètre
        print(f"la voiture a {self.nb_roues} roues")
```

Cette fonction s'utilise donc "avec" un objet :

```python
voiture1.afficher_nb_roues()
```

`voiture1` est bien une variable faisant référence à un objet de de type `voiture`.

On peut "faire la même chose" en écrivant la ligne suivante :

```python
Voiture.afficher_nb_roues(voiture1)
```

On comprend ainsi que le mot clef `self`, à l'intérieur d'une classe, à faire référence à l'objet en lui-même.

Revenons maintenant au début du fichier. On trouve avant le constructeur la ligne suivante : 

```python
nb_voitures = 0
```

Il s'agit d'un attribut de classe. Contrairement à un attribut d'instance, un attribut de classe n'existe "qu'une seule fois".

Pour manipuler des attributs de classe, on peut écrire des méthodes de classe :

```python
    def afficher_nb_voitures(cls):  # méthode de classe : cls en paramètre
        print(f"{cls.nb_voitures} voitures ont été crées")  # nb_voitures est bien un attribut de la classe Voiture
```

Dans une méthode de classe, on utilise `cls` comme paramètre.

NB : `self` et `cls` ne sont pas des mots clefs, mais des conventions de nommage. Il est tout à fait possible d'appeler une méthode de classe via un objet :

```python
voiture1.afficher_nb_voitures()
```

Écrire `self` ou `cls` permet simplement de savoir si une méthode est pensée pour être une méthode d'instance ou de classe. Mais en définitive, une méthode reste une méthode.

On distingue enfin un troisième type de méthodes : les méthodes statiques. Il s'agit de méthodes qui ne "travaillent" ni avec des attributs d'instance, ni avec des attributs de classe. Exemple :

```python
    def convertir_miles_en_km(miles):  # méthode statique
        return miles * 1.609344
```

Pour finir, parlons de la méthode d'instance `__str__()`. Il s'agit d'une méthode qui va définir la chaîne de caractère affiché via la fonction `print()`

```python
    def __str__(self):
        return f"la voiture a {self.nb_roues} roues et {self.nb_chevaux} chevaux"
```

Essayez maintenant de faire un print de `voiture1` :

```python
print(voiture1)
```

Vous verrez ainsi dans la console : `la voiture a 4 roues et 4 chevaux`.

L'ensemble de cette exemple est consultable dans la fichier "demo_class.py".

## Exercice : gestionnaire de *login* v4

Essayez d'implémenter le gestionnaire de *login* en tant que classe, qui possédera un ou plusieurs attribut(s) et méthodes d'instance.

## La POO : l'héritage

L'héritage est un concept fondamental de la POO. Il s'agit de la possibilité de créer des **classes filles** à partir de **classes mères**. La classe fille va hériter des membres (attributs et méthodes) de la classe mère, mais pourra en définir de nouveau.

Exemple :

```python
class Animal:

    def __init__(self, taille, poids):
        self.sentient = True
        self.taille = taille
        self.poids = poids

    def __str__(self):
        return f"l'animal pèse {self.poids} kg et mesure {self.taille} cm"


class Mammifere(Animal):

    def __init__(self, taille, poids, nb_mammelles):
        super().__init__(taille, poids)
        self.nb_mammelles = nb_mammelles

    def __str__(self):
        return f"le mammifère pèse {self.poids} kg, mesure {self.taille} cm et a {self.nb_mammelles} mammèles"
```

La classe `Mammifere` est une classe fille de la classe `Animal`. Cela signifie qu'un objet de type `Mammifere` possédera des attributs `sentient`, `taille`, `poids`, hérités de la classe `Animal`. L'attribut `nb_mammelles` est lui un attribut de la classe `Mammifere`.

Expliqué autrement, on peut dire que tout les mammifères sont des animaux, mais tous les animaux ne sont pas des mammifères.

Dans le constructeur (c'est à dire la méthode `__init__()`) de la classe `Mammifere`, on utilise la méthode `super()`. Celle-ci permet de faire référence à la classe mère. En l’occurrence, la ligne en question permet d'appeler la méthode `__init__()` de la classe `Animal`.

Il y aurait encore beaucoup de choses à dire sur la POO en Python. Mais il s'agit de concepts un peu plus avancés, qui demandent du temps et beaucoup de pratique pour être bien maîtrisés. Ici, le but est plutôt de maîtriser les rudiments de la POO, qui interviennent dans la partie suivante.

## Les interfaces graphiques

Nous allons voir dans cette partie comment réaliser un *GUI* (pour *Graphical User Interface*), à l'aide du *framework* `wxPython`.

Mais avant de commencer, que signifie *framework* ? C'est un terme que l'on entend souvent en informatique, mais sa définition est parfois un peu floue. Comme pour un module, on peut voir ça comme une "boit à outils". Mais alors, quelle est la différence entre un module et un *framework* ? En Python, on distingue quatre types de "boites à outils" :

- Le module : il s'agit d'un (et d'un seul) fichier `.py` pouvant définir fonctions classes et variables. `random`, `time`... sont des modules. 

- Le *package* : il s'agit d'un dossier contenant plusieurs modules (donc plusieurs fichiers). `numpy` et `pandas` sont des *packages*.

- La bibliothèque (*library* en anglais) : il s'agit d'un ensemble de plusieurs *packages* et modules. `matplotlib` est une librairie.

- Le *framework* : comme une librairie, c'est un ensemble de *packages* et de modules. On parle de *framework* plutôt que de bibliothèques quand le contenu de celle-ci est "plus complexe", et surtout quand celui-ci contient tous les éléments permettant de réaliser une application spécifique. Exemple : `django` et `flask` pour les applications web, `circuit python` pour l'électronique...

Ces termes sont parfois utilisés de manière interchangeable par certaines personnes. Il est donc bon de les connaître, et de savoir qu'ils désignent tous plus ou moins la même chose.

Contrairement aux modules comme `random` ou `datetime`, `wxPython` n'est pas inclus nativement dans Python. Il faut donc l'installer sur sa machine. 

Normalement, si vous écrivez `import wxPython` au début d'un fichier avec PyCharm, L'IDE devrait vous proposer de gérer lui-même l'installation du module en question (fonctionne sur Linux, mais à vérifier sur Windows).

Sinon, il faudra passer par l'utilitaire `pip`, le *package installer* de Python

Suivez ces tutoriels pour vérifier si `pip` est installé sur votre machine, puis pour installer `wxPython` via `pip` :

https://pip.pypa.io/en/stable/getting-started/

https://www.wxpython.org/pages/downloads/

Le fichier "demo_wx.py" contient un premier exemple de fenêtre :

```python
import wx

app = wx.App()
window = wx.Frame(None, title="Encodage des notes", size=(600, 600))
panel = wx.Panel(window)
titre = wx.StaticText(panel, label="Fenêtre d'encodage des notes", pos=(200, 10))
matiere1 = wx.StaticText(panel, label="Économie", pos=(50, 50))
note1 = wx.TextCtrl(panel, pos=(200, 50))
matiere2 = wx.StaticText(panel, label="Maths", pos=(50, 100))
note2 = wx.TextCtrl(panel, pos=(200, 100))
matiere3 = wx.StaticText(panel, label="Anglais", pos=(50, 150))
note3 = wx.TextCtrl(panel, pos=(200, 150))
btn_moy = wx.Button(panel, label="Calculez la moyenne", pos=(200, 200))
window.Show(True)
app.MainLoop()
```

- `wx.App()` sert à créer un objet de type "application".

- `wx.Frame()` sert à créer un conteneur. Une *frame* est composé d'une barre de titre, de bordures et d'un conteneur central.

	La notion de conteneur est fondamental lorsque l'on développe des interfaces graphiques. On peut voir ça comme un "espace" dans lequel on peut venir placer divers éléments.

- `wx.Panel()` sert à créer un *panel*. Il s'agit aussi d'un conteneur. Dans cet exemple, le *panel* sera automatiquement placé dans la *frame*.

- `wx.staticText()` permet de créer un espace de texte. `wx.TextCtrl()` permet de créer un champ de texte (où l'on peut écrire quelque chose).

- `wx.Button()` permet de créer un bouton.

- `window.Show(True)` permet d'afficher la *frame* (ici référencé par la variable `window`)

- `app.MainLoop()` permet de lancer l'application. En effet, le programme va tourner en boucle, pour que la fenêtre reste affiché tant que le programme n'est pas terminée.

Vous pouvez lancer ce script. Vous verrez ainsi la fenêtre apparaître, avec les lignes de textes, les champs et le bouton.

Néanmoins, ce script ne présente pas grand intérêt. Certes, il affiche une fenêtre, mais c'est tout. Vous pouvez cliquer sur le bouton, ou écrire du texte dans les champs, mais il ne se passera rien.

Pour rendre la fenêtre interactive, il faudra écrire un programme qui réagira à des évènements.

Une interface graphique fonctionne tant que le programme reste dans la *MainLoop*. Comme pour une boucle `while`, le programme exécutera la même chose en boucle. Le principe d'un évènement (*event* en anglais) est de venir interrompre cette boucle, pour exécuter des instructions spécifiques à l'évènement qui vient d'avoir lieu.

Cela peut paraître compliqué, mais c'est ce qu'il se passe tout le temps lorsque vous utilisez un programme avec une interface graphique sur votre ordinateur. Les clics, sur les différents boutons sont des évènements, qui déclenchent une action spécifique en fonction du bouton. Idem si vous faites tourner la molette de votre souris, ou que vous déplacez une fenêtre... La majorité du temps, le programme tourne en boucle et "ne fait rien", et c'est lorsqu'un évènement est déclenché qu'il va effectuer une action particulière.

Pour "lier" une fonction à un évènement, on utilise la méthode `Bind()`.

En Python, on utilise la plupart du temps la programmation orientée objet pour réaliser des interfaces graphiques :

Reprenons l'exemple d'au dessus, mais en rendant le bouton de calcul de la moyenne fonctionnel :

```python
import wx

class PanelNotes(wx.Panel):

    def __init__(self, parent):
        super(PanelNotes, self).__init__(parent)
        self.titre = wx.StaticText(self, label="Fenêtre d'encodage des notes", pos=(200, 10))
        self.matiere1 = wx.StaticText(self, label="Économie", pos=(50, 50))
        self.in_note1 = wx.TextCtrl(self, pos=(200, 50))
        self.matiere2 = wx.StaticText(self, label="Maths", pos=(50, 100))
        self.in_note2 = wx.TextCtrl(self, pos=(200, 100))
        self.matiere3 = wx.StaticText(self, label="Anglais", pos=(50, 150))
        self.in_note3 = wx.TextCtrl(self, pos=(200, 150))
        self.btn_moy = wx.Button(self, label="Calculez la moyenne", pos=(50, 200))
        # Ci-dessous : on lie l'évènement click bouton à la méthode click_btn_moy()
        self.btn_moy.Bind(wx.EVT_BUTTON, self.click_btn_moy)
        self.moyenne = wx.StaticText(self, pos=(200, 200))

    def click_btn_moy(self, e):
        note1 = float(self.in_note1.GetLineText(0))
        note2 = float(self.in_note2.GetLineText(0))
        note3 = float(self.in_note3.GetLineText(0))
        moyenne = (note1 + note2 + note3)/3
        self.moyenne.SetLabel(str(moyenne))

class FrameNotes(wx.Frame):

    def __init__(self, parent):
        super(FrameNotes, self).__init__(parent)
        self.p = PanelNotes(self)
        self.Show(True)

appli = wx.App()
FrameNotes(None)
appli.MainLoop()
```


Évidemment, il existe beaucoup d'objets dans le module wxPython, qui permettent de réaliser des interfaces très "complexes". Mais l'intérêt ici est de comprendre la notion d'évènement, car peu importe la complexité d'une interface, son fonctionnement reposera sur ceux-ci.

## Exercice : gestionnaire de *login* : GUI

Essayez d'utiliser votre classe de gestionnaire de *login* dans une interface graphique.

## Exercice : cycle jour nuit : GUI

Essayer de créer une interface graphique affichant le cycle jour nuit.

## Les modules de *Data Science* : `numpy`, `pandas`, `matplotlib`

*Data Science* : science des données. Comme son nom le laisse supposer, c'est un discipline dédiée au traitement et à l'analyse de grandes quantités de données.

On "fait" de la *Data Science* dans beaucoup de domaines : sciences physiques (phénomènes météo, physique nucléaire...), informatique (*Big Data*, intelligence artificielle), économie... Peu importe ce qu'une donnée représente derrière, on travaille de toute façon toujours un peu de de la même manière avec.

Python est devenu depuis quelques années un langage fortement utilisé pour faire de la *Data Science*. Notamment grâce à `NumPy`, `pandas` et `matplotlib`.

De quoi s'agit-il ? Et bien d'outils fournissant des fonctions, des objets, des variables... facilitant le traitement, l'analyse et l'affichage de données.

Pour être précis du point de vue terminologique, `NumPy` et `pandas` sont des *packages*, et `matplotlib` est une bibliothèque (*library* en anglais).

NB : si vous voulez être sur d'avoir la bonne info sur une "boite à outil", allez consulter son dépôt GitHub. À priori, l'info provenant des développeur·euse·s est la plus fiable.

## NumPy

NumPy est l'acronyme de *Numeric Python*.

Pour utiliser NumPy dans son programme, il faut l'installer pour pouvoir l'importer. Écrivrez cette ligne en haut d'un fichier Python :

```python
import numpy as np
```

Faites clic-droit sur `numpy` puis "show context action" -> "install package numpy".

Utiliser l'alias `np` pour NumPy est une convention.


Alors, à quoi sert NumPy ? À créer et manipuler des *arrays*. Une *array*, ou tableau en français, est un structure de données. On peut se représenter ça comme une "grille" de données, avec une position et des manières de travailler avec ces éléments.

On pourrait tout à fait donner cette description pour une liste. Quelles sont donc les différences entre une liste et une *array* ?

Et bien, une liste est une structure homogène. Cela signifie que tout ses éléments sont du même type, là où une liste peut contenir à la fois des `int`, des `float`, des `str` ou tout autre type.

Cette contrainte en fait un objet particulièrement adapté pour des opérations mathématiques. Car comme son nom le laisse deviner, NumPy est pensé pour manipuler des nombres.

Cette "contrainte" engendre plusieurs avantages. Le premier, c'est que manipuler un *array* NumPy est beaucoup plus rapide que manipuler une liste. Le second, c'est que c'est une séquence plus compacte, c'est à dire qu'elle prend moins de place en mémoire.

### Créer et lire un *array*

Comment créer un *array* à partir de valeurs connues ? Via la méthode `array()` :

```python
a = np.array([1, 2, 4, 8, 16])
print(a)
```

Affichons maintenant le type de `a` (`print(type(a))`) :

La console affiche `<class 'numpy.ndarray'>`. Que signifie `nd` : *N-dimensionnal*; de dimension N en français. On parle donc de tableaux multidimensionnels.

Un tableau à une dimension peut être visualisé comme une colonne, un tableau à deux dimensions comme une carré (ou une feuille d'un tableur Excel), et ainsi de suite.

NB : il est tout à fait possible de faire des listes de N dimensions, en insérant des listes dans des listes.

On peut créer rapidement un *array* "vide", rempli de 0 ou de 1 :

```python
empty = np.empty(2)
zeros = np.zeros(5)
ones = np.ones(10)
```

Vous avez normalement déjà utilisé la fonction `range()`. Il existe des méthodes servant à faire la même chose dans NumPy :

```python
even_digits = np.arange(0, 10, 2)
mult_of_four = np.linspace(0, 41, num=10)
```

Il est possible de spécifier le type de données de l'*array* :

```python
digits = np.arange(0, 10, dtype=np.int64) # integer on 64 bits
```

Comment récupérer un élément d'un *array* : Comme pour une liste

```python
print(digits[1])
```


Comment créer un *array* 2D :

```python
tab = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
```

ATTENTION : la méthode `array()` ne pend qu'un argument. Ici, vous voyez que les 3 tuples de 3 éléments sont elles-mêmes placées dans un tuple, qui est passé en argument de la méthode.

Comment récupérer un élément d'un *array* multidimensionnel : là encore, comme pour une liste :

```python
print(tab[2, 1])
```

Il est également possible de faire de lecture conditionnelle :

```python
print(tab[tab % 2 == 0])
```

### Ajouter, supprimer et trier

Voici deux méthodes pour ajouter des éléments : 

```python
a = np.array([2, 4, 6, 8, 10])
a_bis = np.append(a, [12, 14])

b = np.array([20, 30, 40, 50])
c = np.concatenate((a, b))
```

Bien que l'on parle d'ajout, ces deux méthodes ne modifient pas l'*array* de départ : elles en renvoient un nouveau à partir des arguments passés en entrée.

Voici deux méthodes pour supprimer des éléments :

```python
a_minus = np.delete(a, [2, 4])

d = a[3: 5]
```

Voici la méthode pour trier un *array* :

```python
unsorted = np.array([4, 12, 5, 1, -5, 154])
sorted = np.sort(unsorted)
```

### Les caractéristiques d'un *array*

Il est possible de connaître les dimensions, la taille et la forme d'un *array* :

```python
a = np.array([[[0, 2, 4],
               [1, 3, 5]],

              [[5, 10, 15],
              [10, 20, 30]],

              [[15, 25, 20],
              [0.5, 15.5, 55]]])

print(f"dimensions : {a.ndim}, taille : {a.size}, forme : {a.shape}")
```

### Itérer sur un *array*

On peut itérer sur un *array* avec une boucle `for` : 

```python
a = np.array([np.arange(0, 10, 2), np.arange(10, 20, 2), np.arange(20, 30, 2)])
print(a)
for x in a:
    print(f"x = {x}")
```

Sauf qu'ici, pour afficher tous les éléments de l'*array* un par un, il faudrait pour chaque ligne faire encore une fois une boucle `for`. Pour éviter cela, on peut utiliser une méthode de `ndarray`; `nditer()` :

```python
for x in np.nditer(a):
    print(f"nditer : x = {x}")
```

### Modifier la forme d'un *array*

Modifier la forme d'un *array* signifie obtenir un *array* possédant les mêmes données mais ayant une forme (nombres de lignes, de colonnes) différente. Exemple :

```python
m32 = np.array([[1, 2],
                [3, 4],
                [5, 6]])

m23 = m32.reshape(2, 3)

# Other method
m23 = numpy.reshape(m32, newshape=(2, 3))
```

### Augmenter la dimension d'un *array*

Il est possible de créer des *arrays* de dimension N+x à partir d'un *array* de dimension N. Par exemple :

```python
a = np.arange(0, 11, 2)
print(a.shape)

cv = a[:, np.newaxis]
print(cv.shape)

rv = a[np.newaxis, :]
print(rv.shape)
```

`a` est un *array* de dimension 1. `cv`, même s'il s'agit d'un vecteur colonne, est quand même de dimension 2. Idem pour `rv` qui est un vecteur ligne.

### Créer des *arrays* à partir d'*arrays* existants

Nous avons déjà vu comment "extraire" un *array* à partir d'un autre. Mais on peut également "*stacker*" des *arrays* :

```python
avs = np.vstack((a1, a2))
print(avs)
print()
ahs = np.hstack((a1, a2))
```

Inversement, on peut "*spliter" des *arrays* :

```python
avs_split = np.split(avs, 2)
```

### Opérations mathématiques

Il est possible d'effectuer des opérations mathématiques sur des *arrays* : 

```python
tab = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
tab2 = tab + np.ones(3, 3)

tab3 = tab2 * tab

tab4 = tab2 / tab3

tab_brdcst = tab * 4
```
res = np.array((np.nan))
Pour effectuer la somme  des éléments d'un *array*, on utilise la méthode `sum()` :

```python
tab_sum = tab.sum()

tab_r_sum = tab.sum(axis=0)
tab_c_sum = tab.sum(axis=1)
print(f"row sum : {tab_r_sum}, column sum : {tab_c_sum}")
```

### Autres méthodes utiles 

- `min()`
- `max()`
- `random()`
- `unique()`
- `flip()`
- `flatten()`
- `ravel()`

### Charger et sauvegarder des objets NumPy

Il est possible de sauvegarder un *array* dans un fichier `.npy`, afin de pouvoir le réutiliser plus tard. Le principe est le même qu'avec `open()` :

```python
np.save("basicTable", tab)
loaded_tab = np.load(basicTable.npy")
```

## Exercice : Ranger des nombres

Créez un *array* via l'instruction suivante :

```python
a = np.random.randint(0, 1000, 100)
```

Essayez, à partir de celui-ci, de créer un *array* de forme (10, x) contenant dans chaque ligne les nombres compris dans les intervalles [0, 100[, [100, 200[... et ainsi de suite.

*tip* : il n'y aura vraisemblablement pas la même quantité de nombres pour chaque intervalle. Pour représenter une case vide, vous pouvez utiliser `np.nan` (`nan` : not a number).

## pandas

Comme pour n'importe quel module/*package*/*library*, il faut importer pandas pour l'utiliser : 

```python
import pandas as pd
```

### Le type *Series*

Le package repose sur deux types : les `series` et les `DataFrame`. Voici comment créer une `series` :

```python
s = pd.Series([0, 8, 2, 3, 55])
print(s)
```

La console va afficher en sortie :

```python
0     0
1     8
2     2
3     3
4    55
dtype: int64
```

De quoi s'agit-il ? Et bien, une série, c'est un *array* à 1 dimension + des *labels* appelées *index*. Dans le *print* ci-dessus, les données de la *series* sont affichées à droite, et les *index* à gauche.

Les *series* de pandas reposent sur les *arrays* de NumPy. On peut créer une *series* a partir d'un *array* :

```python
a = np.array([1, 2, 4])
s2 = pd.Series(a)
```

### Le type *DataFrame*

Abordons le maintenant la *data frame*. Pour faire simple, c'est l'équivalent en Python d'un tableur Excel. Chaque ligne et chaque colonne est accessible via un *label*.

Par exemple, regardons comment créer un *data frame* ayant pour *indexs* (*labels* des lignes) des dates :

```python
dates = pd.date_range("20211207", periods=4)
df = pd.DataFrame(np.random.randn(4, 4), index=dates, columns=list("1234"))
```

Vous pouvez voir qu'à droite de chaque ligne sont affichées les dates (qui ont été passé en *index*) , et au-dessus de chaque colonne les chiffres `1 2 3 4` qui ont été passé au paramètre `columns`.

On peut également créer un *data frame* à partir d'un dictionnaire : Les clefs deviendront les noms de colonnes.

```python
dico = {"fruits": [1, 2, 4, 6], "legumes": [2, 4, 10, 15], "feculents": [45, 10, 2, 4]}
df2 = pd.DataFrame(dico, index=dates)
```

NB : On ne peut pas passer utiliser n'importe quel dictionnaire pour créer un *data frame*. Il faut bien sûr que pour chaque clef, la valeur soit une séquence homogène. Par contre une colonne peut contenir un type donnée (par exemple des `int`, une autre des `str`...).

### Récupérer des informations d'un *data frame*

Voici les méthodes et attributs les plus couramment utilisées :

- `df.head()` : renvoie les 2 premières lignes
- `df.tail(5)` : renvoie les 5 dernières lignes
- `df.index` : attribut contenant les *index* (noms de ligne)
- `df.columns` : attribut contenant les noms de colonne
- `df.to_numpy` : renvoie un *array* contenant les données du *data frame*

### Extraire des données

Récupérer les données d'une colonne :

```python
df2_c1 = df2["legumes"]
```

Récupérer les données de certaines lignes :

```python
df3_r01 = df3[0:2] # return firt two lines of df3
```

Récupérer plusieurs axes :

```python
df2_select = df2.loc[[dates[0],dates[2]], ["fruits", "feculents"]]
```

Ici on affichera les données des lignes 0 et 2 pour les colonnes `fruits` et `feculents`.

Si l'on ne connaît pas les noms des labels de nos lignes et colonnes, on peut utiliser la méthode `iloc` (`i` pour *index*). La syntaxe est la même qu'avec `loc`, il faut simplement travailler avec les indices des lignes et colonnes, non pas avec les *labels* :

```python
df2_iselect = df2.iloc[[0, 2], [0, 2]] # Equivalent to previous line
```

Comme pour les *arrays*, on peut utiliser des conditions :

```python
print(df3 > 0)
print(df3.iloc[:, 2] > 0)
```

### Modifier des données

Un *Data Frame* est un type mutable. Comme pour la localisation, on peut modifier une case de notre *Frame* soit via les labels, sont via les numéros de ligne/colonne :

```python
df2.at["20211208", "feculents"] = 20
df3.iat[49, 4] = 100
```

Comme pour les *arrays*, il est possible de concaténer des *data frame* : 

```python
p1 = pd.DataFrame(np.random.randn(5, 3))
p2 = pd.DataFrame(np.random.randn(5, 3))
p = pd.concat((p1, p2))
```


### NaN : *data* manquante

Dans l'exercice de tri sur NumPy, nous avons parlé de *nan* pour *not a number*. 
Son utilisation fait aussi sens avec des *data frame*.

En effet, pour une quelconque raison, toutes les cases d'un *data frame* ne seront pas forcément remplies (capteur en panne, connexion interrompue...). une case vide contiendra `np.nan` :

```python
df4 = df2.reindex(index=dates[0:3], columns=list(df2.columns) + ["condiments"])
```

`reindex` permet de redéfinir les index : ici suppression d'une ligne et ajout d'une colonne. `condiments` ne contient pour l'instant que des `np.nan`.

Deux méthodes peuvent être pratique pour se débarasser des `np.nan` :

- `df.dropna()` : renvoie la *data frame* sans les lignes contenant des NaN
- `df.fillna(value=x)` : remplace les NaN par la `value` passé en argument

### Opérations mathématiques

- `df.mean()` : pour calculer des moyennes
- `df.apply(fct)` : permet d'appliquer des fonctions mathématiques à la *data frame*. Souvent, la fonction passé en paramètre sera une fonction de NumPy.

### Lire et écrire dans un CSV

Pandas fournit des méthodes très simples d'utilisation pour lire et écrire dans un fichier csv :

```python
df2.to_csv("ventes.csv")
df_csv = pd.read_csv("ventes.csv")
```

## Exercice : Bases de données des villes de Belgique

Le format *data frame* est pratique pour servir de base de données. Essayez donc de créer un *data frame* avec, pour les grandes villes du pays, à chaque fois :

- Le code postal
- Le nombre d'habitants
- la superficie
- Autre si vous le souhaitez

Puis créez un petit GUI permettant d'afficher des infos soit sur une ville en particulier, soit sur des statistiques globales (par exemple la moyenne des habitants pour toutes les villes, ou la surface moyenne).

PS : si vous ne maîtrisez pas trop les GUI, vous pouvez le faire via un menu console en utilisant `input("")`.

## matplotlib

Comme pour NumPy et pandas, il faut installer matplotlib pour pouvoir l'importer et l'utiliser :

```python
import matplotlib.pyplot as plt
```

matplotlib permet de créer très simplement des graphiques :

```python
y = np.array([2, 4, 6, 8, 10])
plt.plot(y)
plt.ylabel("array y")
plt.show()
```

Ce code va afficher ce qu'on appelle une figure. Le graphique présenté affiche l'*array* `y` en ordonnée, avec les valeurs en abscisse par défaut (pas de 1). Si l'on veut spécifier un autre pas en abscisse, il faut passer une deuxième séquence en argument :

```python
y = np.array([2, 4, 6, 8, 10])
x = np.array([10, 20, 50, 100, 250])
plt.plot(x, y)
plt.ylabel("array y")
plt.xlabel("array x")
plt.show()
```

Il est possible de fixer la taille des axes :

```python
y = np.array([2, 4, 6, 8, 10])
x = x = np.array([10, 20, 50, 100, 250])
plt.plot(x, y)
plt.ylabel("array y")
plt.xlabel("array x")
plt.axis([0, 300, 0, 12])
plt.show()
```

Il est également possible de tracer plusieurs *arrays* sur un même graphique :

```python
y = np.array([2, 4, 6, 8, 10])
y2 = np.array([4, 8, 12, 16, 20])
y3 = np.random.randint(0, 10, 5)
x = x = np.array([10, 20, 50, 100, 250])
plt.plot(x, y, "xg", x, y2, "sr", x, y3, "b--")        # x : show crosses for points, g : green color
plt.ylabel("arrays y")
plt.xlabel("array x")
plt.axis([0, 300, 0, 25])
plt.show()
```

matplotlib permet de facilement tracer des données contenues dans un dictionnaire ou un *data frame* via les clefs et labels. Exemple :

```python
dico = {"objet n°": (21, 45, 32, 75, 62), "quantitee vendue": (4, 8, 3, 7, 4)}
df = pd.DataFrame(dico)
plt.scatter("objet n°", "quantitee vendue", data=df)
plt.ylabel("quantitée vendue")
plt.xlabel("n° de l'objet")
plt.show()
```

Il est également possible de tracer plusieurs graphiques en une seule figure :

```python
plt.figure()
plt.subplot(121)
plt.bar(x, y, width=10)
plt.subplot(122)
plt.plot("objet n°", "quantitee vendue", data=dico)
plt.suptitle("Deux graphiques en une figure")
plt.show()
```

Et on peut même créer plusieurs figures avec un seul programme :

```python
plt.figure(1)
plt.subplot(121)
plt.title("this is plot 1 of fig 1")
plt.bar(x, y, width=10)
plt.subplot(122)
plt.title("this is plot 2 of fig 1")
plt.plot("objet n°", "quantitee vendue", data=dico)
plt.suptitle("Deux graphiques en une figure")
plt.show()

plt.figure(2)
plt.scatter([1, 2, 3, 4, 5], y3)
plt.title("Here comes fig 2")
plt.show()
```

Il est possible de réaliser des figures et des graphiques via une approche d'avantage orientée objet :

```python
fig1 = plt.figure()
ax1 = fig1.add_subplot(2, 1, 1)
ax1.plot(x, y)
ax2 = fig1.add_subplot(2, 1, 2)
ax2.plot(x, y2)
ax2.annotate("annotation flèchée" , xy=(100, 16), xytext=(150, 10), arrowprops=dict(facecolor="red", shrink=0.05))
plt.show()
```

Il est également possible de sauvegarder une figure dans un fichier image :

```python
fig1.savefig("figure1.png")
```


## Projet d'examen : Traitement et affichage de données Covid

Pour commencer, téléchargez au format .csv le dataset disponible via le lien suivant :

https://www.ecdc.europa.eu/en/publications-data/data-daily-new-cases-covid-19-eueea-country

Il faudra, en utilisant entre autres pandas et matplotlib, tracer une courbe précise.

Chaque étudiant·e devra tracer une courbe différente. Les sujets sont consultables dans le fichier [2021-2022_Sujets.md](2021-2022_sujets.md)