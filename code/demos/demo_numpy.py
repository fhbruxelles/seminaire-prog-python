import numpy
import numpy as np

# Create and read

a = np.array([2, 4, 8, 16])
print(a)
print(type(a))

empty = np.empty(2)
zeros = np.zeros(5)
ones = np.ones(10)
print(empty)
print(zeros)
print(ones)

even_digits = np.arange(0, 10, 2)
mult_of_four = np.linspace(4, 40, num=10)
print(even_digits)
print(mult_of_four)

digits = np.arange(0, 10, dtype=np.int64)
print(digits)

print(digits[1])

tab = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
print(tab)

print(tab[2, 1])

print(tab[tab % 2 == 0])

# Add, remove and sort
print()
a = np.array([2, 4, 6, 8, 10])
a_bis = np.append(a, [12, 14])
print(a_bis)

b = np.array([20, 30, 40, 50])
c = np.concatenate((a, b))
print(c)

a_minus = np.delete(a, [2, 4])
print(a_minus)

d = a[3: 5]
print(d)

unsorted = np.array([4, 12, 5, 1, -5, 154])
sorted = np.sort(unsorted)
print(sorted)

a = np.array([[[0, 2, 4],
               [1, 3, 5]],

              [[5, 10, 15],
               [10, 20, 30]],

              [[15, 25, 20],
               [0.5, 15.5, 55]]])

print(f"dimensions : {a.ndim}, taille : {a.size}, forme : {a.shape}")

#Iterate
print()
a = np.array([np.arange(0, 10, 2), np.arange(10, 20, 2), np.arange(20, 30, 2)])
print(a)
for x in a:
    print(f"x = {x}")

for x in np.nditer(a):
    print(f"nditer : x = {x}")

# Modify the shape of an array
print()
m32 = np.array([[1, 2],
                [3, 4],
                [5, 6]])
print(m32); print()

m23 = m32.reshape(2, 3)
print(m23); print()

# Other method
m23 = numpy.reshape(m32, newshape=(2, 3))
print(m23)

# Expand an array
print()
a = np.arange(0, 11, 2)
print(a.shape)
print(a)

cv = a[:, np.newaxis]
print(cv.shape)
print(cv.ndim)
print(cv)

rv = a[np.newaxis, :]
print(rv.shape)
print(rv.ndim)
print(rv)

# stack arrays
print()
a1 = np.array([1, 2, 3, 4, 5, 6])
a2 = np.array([100, 99, 98, 97, 96, 95])

avs = np.vstack((a1, a2))
print(avs)
print()
ahs = np.hstack((a1, a2))
print(ahs)

# split arrays
print()
avs_split = np.split(avs, 2)
print(avs_split)

# Mathematic operations
print()
tab = np.array(((1, 2, 3), (4, 5, 6), (7, 8, 9)))
tab2 = tab + np.ones((3, 3))
print(tab2)
tab3 = tab2 * tab
print(tab3)
tab4 = tab2 / tab3
print(tab4)
tab_brdcst = tab * 4
print(tab_brdcst)
print()

tab_sum = tab.sum()
print(tab_sum)
tab_r_sum = tab.sum(axis=0)
tab_c_sum = tab.sum(axis=1)
print(f"row sum : {tab_r_sum}, column sum : {tab_c_sum}")

# Save and load
print()
np.save("basicTable", tab)
loaded_tab = np.load("basicTable.npy")
print(loaded_tab)