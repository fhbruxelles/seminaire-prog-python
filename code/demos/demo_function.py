# variable globales : très peu recommandé
z = 42
msg = "message de base"


def print_bonjour():
    print("bonjour")


def addition(a, b):
    return a + b


def puissance(x, y=2):
    return x ** y


def compter_a(chaine: str):
    nb = chaine.count("a")
    return nb


def deviner_nb():
    print("Vous avez 10 essais pour deviner le nombre secret")
    essais = 10
    guess = int(input("Entrez votre premier guess : "))
    while essais > 0:
        if guess == 13:
            print("Bravo, vous avez deviné le nombre secret !")
            return
        else:
            essais -= 1
            if essais == 0:
                print("C'est perdu")
                return
            else:
                guess = int(input("Plus que " + str(essais) + " tentatives. Entrez une nouvelle proposition : "))



a = print_bonjour()
print(type(a))

somme = addition(4, 8)
print(somme)

b = puissance(3, 4)
c = puissance(4)
print(b)
print(c)

d = puissance(y=3, x=4)
print(d)

nb_a = compter_a("aloha miami")
print(nb_a)
# nb_a = compter_a(2)

i = 0
while i < 100:
    j = i ** 3
    if j % 2 == 0:
        k = j // 5
        print("j est pair")
    print(k)
    i += 1

print(j)

deviner_nb()
