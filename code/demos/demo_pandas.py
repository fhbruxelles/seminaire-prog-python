import pandas as pd
import numpy as np

s = pd.Series([0, 8, 2, 3, 55])
print(s)

a = np.array([40, 10, 25])
s2 = pd.Series(a)
print(s2)

dates = pd.date_range("20211207", periods=4)
df = pd.DataFrame(np.random.randn(4, 4), index=dates, columns=list("1234"))
print(df)

dico = {"fruits": [1, 2, 4, 6], "legumes": [2, 4, 10, 15], "feculents": [45, 10, 2, 4]}
df2 = pd.DataFrame(dico, index=dates)
print(df2)

df3 = pd.DataFrame(np.random.randn(50, 5))
print(df3)
print(df3.head(3))
print(df3.tail(4))

df2_c1 = df2["legumes"]
print(df2_c1)

df3_r01 = df3[0:2]
print(df3_r01)

df3_r2 = df3.loc[2]
print(df3_r2)

df2_r1 = df2.loc[dates[1]]
print(df2_r1)

df2_select = df2.loc[[dates[0],dates[2]], ["fruits", "feculents"]]
print(df2_select)

df2_iselect = df2.iloc[[0, 2], [0, 2]]
print(df2_iselect)

print(df3 > 0)
print(df3.iloc[:, 2] > 0)

df2.at["20211208", "feculents"] = 20
df3.iat[49, 4] = 100
print(df2)
print(df3.tail(2))

p1 = pd.DataFrame(np.random.randn(5, 3))
p2 = pd.DataFrame(np.random.randn(5, 3))
p = pd.concat((p1, p2))
print(p)

df4 = df2.reindex(index=dates[0:3], columns=list(df2.columns) + ["condiments"])
print(df4)

df2.to_csv("ventes.csv")
df_csv = pd.read_csv("ventes.csv")
print(df_csv)
