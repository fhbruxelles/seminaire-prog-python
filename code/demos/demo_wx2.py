import wx

class PanelNotes(wx.Panel):

    def __init__(self, parent):
        super(PanelNotes, self).__init__(parent)
        self.titre = wx.StaticText(self, label="Fenêtre d'encodage des notes", pos=(200, 10))
        self.matiere1 = wx.StaticText(self, label="Économie", pos=(50, 50))
        self.in_note1 = wx.TextCtrl(sepython frame and panellf, pos=(200, 50))
        self.matiere2 = wx.StaticText(self, label="Maths", pos=(50, 100))
        self.in_note2 = wx.TextCtrl(self, pos=(200, 100))
        self.matiere3 = wx.StaticText(self, label="Anglais", pos=(50, 150))
        self.in_note3 = wx.TextCtrl(self, pos=(200, 150))
        self.btn_moy = wx.Button(self, label="Calculez la moyenne", pos=(50, 200))
        self.btn_moy.Bind(wx.EVT_BUTTON, self.click_btn_moy)
        self.moyenne = wx.StaticText(self, pos=(200, 200))

    def click_btn_moy(self, e):
        note1 = float(self.in_note1.GetLineText(0))
        note2 = float(self.in_note2.GetLineText(0))
        note3 = float(self.in_note3.GetLineText(0))
        moyenne = (note1 + note2 + note3)/3
        self.moyenne.SetLabel(str(moyenne))

class FrameNotes(wx.Frame):

    def __init__(self, parent):
        super(FrameNotes, self).__init__(parent)
        self.p = PanelNotes(self)
        self.Show(True)


if __name__ == "__main__":
    appli = wx.App()
    FrameNotes(None)
    appli.MainLoop()
