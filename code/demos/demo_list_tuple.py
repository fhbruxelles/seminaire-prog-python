li = [1, 4.5, "bonjour", True, 785, "hello"]
tu = (1, 4.5, "bonjour", True, 785, "hello")

print(li[0])
print(tu[2])

debut_li = li[0:2]
print(debut_li)

nb_elts = len(li)
print(nb_elts)

li[0] = 45
li.append("hello")
print(li)
li.remove(4.5)
li.remove(li[0])
print(li)