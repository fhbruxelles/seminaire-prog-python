class Animal:

    def __init__(self, taille, poids):
        self.sentient = True
        self.taille = taille
        self.poids = poids

    def __str__(self):
        return f"l'animal pèse {self.poids} kg et mesure {self.taille} cm"


class Mammifere(Animal):

    def __init__(self, taille, poids, nb_mammelles):
        super().__init__(taille, poids)
        self.nb_mammelles = nb_mammelles

    def __str__(self):
        return f"le mammifère pèse {self.poids} kg, mesure {self.taille} cm et a {self.nb_mammelles} mammèles"


animal1 = Animal(100, 35)
print(animal1)
mammifere1 = Mammifere(170, 60, 2)
print(mammifere1)