import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


y = np.array([2, 4, 6, 8, 10])
y2 = np.array([4, 8, 12, 16, 20])
y3 = np.random.randint(0, 10, 5)
x = np.array([10, 20, 50, 100, 250])
# plt.plot(x, y, "xg", x, y2, "sr", x, y3, "b--")        # x : show crosses for points, g : green color
# plt.ylabel("arrays y")
# plt.xlabel("array x")
# plt.axis([0, 300, 0, 25])
# plt.show()

dico = {"prix": (21, 45, 32, 75, 62), "quantite": (4, 8, 3, 7, 4)}
df = pd.DataFrame(dico)
# x = np.array([0, 1, 2, 3, 4])
# plt.scatter("prix", "quantite", data=df)
# plt.ylabel("quantites")
# plt.xlabel("prix")
# plt.show()

# plt.figure(1)
# plt.subplot(121)
# plt.title("this is plot 1 of fig 1")
# plt.bar(x, y, width=10)
# plt.subplot(122)
# plt.title("this is plot 2 of fig 1")
# plt.plot("prix", "quantite", data=dico)
# plt.suptitle("Deux graphiques en une figure")
# plt.show()

# plt.figure(2)
# plt.scatter([1, 2, 3, 4, 5], y3)
# plt.title("Here comes fig 2")
# plt.show()

fig1 = plt.figure()
ax1 = fig1.add_subplot(2, 1, 1)
ax1.plot(x, y)
ax2 = fig1.add_subplot(2, 1, 2)
ax2.plot(x, y2)
ax2.annotate("annotation flèchée" , xy=(100, 16), xytext=(150, 10), arrowprops=dict(facecolor="red", shrink=0.05))
plt.show()
fig1.savefig("figure1.png")

