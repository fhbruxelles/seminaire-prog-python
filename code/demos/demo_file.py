#Lecture d'un fichier

#méthode 1

fp = open("../Datas/data2.txt")   #on ouvre le ficher
data = fp.readlines()   #on stocke les lignes du fichier dans la variable data : data sera une liste ayant autant d'éléments qu'il y a de lignes dans le fichier
print(type(data))
print("file contains : {}".format(data)) #print formaté : permet d'afficher simplement plusieurs données via un seul print
fp.close()              #toujours fermer le fichier si on l'a ouvert plus tot

#méthode 2 : meilleure option, pas besoin de penser à refermer le fichier explicitement, et d'avantage pensée "Python"

fichier = "data.txt"

with open(fichier, "r") as fp:  #ici "r" signifie qu'on accède au fichier en mode lecture (read en anglais)
    data = fp.read()            #lecture simple : on récupère tout le fichier en un seul String
    print(type(data))
    print("file contains : {}".format(data))

#Écriture dans un fichier
note_eco = int(input("Entrez votre note en éco : "))
note_maths = int(input("Entrez votre note en maths : "))
note_anglais = int(input("Entrez votre note en anglais : "))

releve = {"note éco": note_eco, "note maths": note_maths, "note anglais": note_anglais}

bulletin = "bulletin.txt"

with open("bulletin.txt", "a") as fp:        #ouverture du fichier en mode écriture (write en anglais), + signifie créer le fichier s'il n'existe pas
    for matiere, note in releve.items():
        fp.write("{} : {}\n".format(matiere, note)) #écriture du nom de la matière suivi de la note, à chaque fois dans une nouvelle ligne

moyenne = (note_eco + note_maths + note_anglais)/3

with open(bulletin, "a") as fp:             # "a" signifie append, c'est à dire ajouter à la suite du fichier (comme la méthode append pour une liste)
    fp.write("moyenne : {}\n".format(moyenne))