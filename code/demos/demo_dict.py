dico = {6: 4.5, "bonjour": [4, "hello"], 75.23: False, (1, 2, 3): 10}

a = dico[6]
print(a)

dico["hello"] = 55
print(dico)

del dico["bonjour"]
print(dico)