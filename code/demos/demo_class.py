class Voiture:  # définit le nom de la classe. Tout le contenu de la classe doit etre dans le bloc
    nb_voitures = 0  # attribut de classe

    def __init__(self, nb_r, nb_c):  # Méthode : fonction d'une classe
        self.nb_roues = nb_r  # attribut d'instance
        self.nb_chevaux = nb_c
        self.demarre = False
        Voiture.nb_voitures += 1  # ici on modifie l'attribut de classe

    def __str__(self):
        return f"la voiture a {self.nb_roues} roues et {self.nb_chevaux} chevaux"

    def demarrer(self):
        self.demarre = True

    def afficher_nb_roues(self):  # métohde d'instance : self en paramètre
        print(f"la voiture a {self.nb_roues} roues")

    def afficher_nb_voitures(cls):  # méthode de classe : cls en paramètre
        print(f"{cls.nb_voitures} voitures ont été crées")  # nb_voitures est bien un attribut de la classe Voiture
        # print(f"{cls.nb_roues} voitures ont été crées") #erreur : nb_roues est un attribut d'instance et non de classe

    def convertir_miles_en_km(miles):  # méthode statique
        return miles * 1.609344


# instanciation d'un objet de type Voiture, qui aura 4 roues et 4 chevaux.
# Cette ligne appelle la méthode __init__ de la classe Voiture
voiture1 = Voiture(4, 4)
voiture2 = Voiture(6, 16)

# utiliser les méthodes d'instance d'une classe :
voiture1.afficher_nb_roues()
# La ligne ci-dessous effectue la meme chose : mais utiliser celle du dessus est moins "lourd"
Voiture.afficher_nb_roues(voiture1)

voiture1.demarrer()
print(voiture1.demarre)
print(voiture2.demarre)

# appel d'une méthode de classe :
Voiture.afficher_nb_voitures(Voiture)

voiture1.afficher_nb_voitures() # on peut appeler une méthode de classe via une instance

# appel d'une méthode statique :
print(Voiture.convertir_miles_en_km(10))

print(voiture1)
