print("hello world") #fonction print, pour afficher une ligne de texte dans la console

print(5+10)     # + : opérateur mathématique
x = 5           # x est une variable référençant vers l'objet de type int valant 5
y = 10
z = x + y       # z référence vers un int valant 15
print(y)


str1 = "hello world "   # objet String : chaine de caractères
premier_char = str1[0]      # on récupère le premier caractère de la chaine : indice 0
troisieme_char = str1[2]    # on récupère le 3eme
dernier_char = str1[-1]     #indice négatif : permet de parcourir la chaine dans le sens inverse
print(premier_char)
print(troisieme_char)
print(dernier_char)

str2 = 'bonjour les "amis" ' # chaine avec des apostrophes
str3 = """j'espère que vous allez "bien\n moi oui""" # chaine avec des triple guillemets
print(str2 + str3) #concaténation de deux Strings

a = 5 < 6  # cette opération de comparaison renvoie un booléen
print(a)

a = 5 < 6 and 10 < 6 # and : opérateur logique
print(a)
