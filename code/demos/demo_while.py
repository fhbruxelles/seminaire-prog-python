n = 0
while n < 10:
    print("n = " + str(n))
    n += 1  # affection et modification en 1 instruction : revient à faire n = n + 1
print("Programme sorti de la boucle while")

nom = input("Entrez votre nom : ")
print("Bonjour " + nom)
