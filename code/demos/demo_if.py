a = 5
if a == 5:
    print("a est égal à 5")

print("print en dehors du bloc")

b = 10
if b == 10:
    print("b vaut 10")
elif b == 15:
    print("b vaut 15")
else:
    print("b ne vaut ni 10 ni 15")

c = 20
if c > 10:
    print("c est strictement supérieur à 10")
if c > 5:
    print("c est strictement supérieur à 5")
