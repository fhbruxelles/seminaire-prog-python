import time
import datetime

def detect_saison(d: datetime.date):
    saisons = ("été", "automne", "hiver", "printemps")
    saison = 0
    if 3 <= d.month < 6:
        saison = saisons[3]
    elif 6 <= d.month < 9:
        saison = saisons[0]
    elif 9 <= d.month < 12:
        saison = saisons[1]
    else:
        saison = saisons[2]

    return saison

def afficher_cycle(saison: str):
    horaires = ((7, 21), (9, 19), (10, 17), (8, 20))
    if saison == "été":
        horaire = horaires[0]
    elif saison == "automne":
        horaire = horaires[1]
    elif saison == "hiver":
        horaire = horaires[2]
    else:
        horaire = horaires[3]
    for i in range(0, 24):
        if i < horaire[0] or i >= horaire[1]:
            print(f"Il est {i} heures. Il fait nuit")
            time.sleep(0.5)
        else:
            print(f"Il est {i} heures. Il fait jour")
            time.sleep(0.5)


jour = int(input("Entrez le jour : "))
mois = int(input("Entrez le mois : "))
annee = int(input("Entrez l'année : "))
d = datetime.date(day=jour, month=mois, year=annee)

saison = detect_saison(d)
afficher_cycle(saison)




