temperature = -40

if temperature >= 40:
    print("Il fait très chaud !")
elif temperature < -30:
    print("Il fait très froid !")
elif temperature > 0:
    print("La température est positive")
elif temperature < 0:
    print("La température est négative")
else:
    print("Il fait 0°C")