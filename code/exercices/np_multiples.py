import numpy as np
import math

a = np.random.randint(0, 1000, 100)
nb_inter = 10
a = np.sort(a)
print(f"{a}\n")

sizes = np.array([])
for i in range(0, nb_inter):
    inter = np.sort(a[(i * 100 < a) & (a < (i + 1) * 100)])
    print(inter)
    print(f"{inter.size}")
    sizes = np.append(sizes, [inter.size])
    print(f"{sizes}\n")

sizes = sizes.astype(int)
print(f"sizes : {sizes}")
print(f"max size = {sizes.max()}\n")

res = np.zeros(sizes.max())
for i in range(0, nb_inter):
    inter = np.array((np.sort(a[((a < (i + 1) * 100) & (a > i * 100))])))
    nans = np.zeros(sizes.max() - sizes[i])
    nans[:] = np.nan
    inter = np.concatenate((inter, nans))
    print(f"temp : {inter}")
    res = np.vstack((res, inter))
    print(f"{res}\n")

res = res[1:, :]

print(f"final 10 rows array :\n{res}")
