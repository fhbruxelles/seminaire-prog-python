# Séminaire de programmation en langage Python

Bienvenue sur le dépôt Git dédiée au séminaire de programmation !

L'objectif de ce séminaire est d'apprendre les bases de la programmation, via le langage Python.

Vous pouvez constater qu'au dessus de ce texte ce trouve un encadré avec des noms de dossiers de fichiers.

Le "syllabus" de ce séminaire se trouve dans le dossier /documentation. Vous pouvez d'ores et déjà cliquer dessus pour le lire, et commencer à étudier.

Le dossier /code contient deux sous dossiers, un contenant les scripts de démo (qui sont a réécrire ou consulter pendant la lecture du syllabus), et un contenant les corrigés des exercices (dont les énoncés sont également dans le syllabus)

Bonne lecture !

